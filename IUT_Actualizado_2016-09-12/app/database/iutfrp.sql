-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-02-2017 a las 00:22:16
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `iutfrped_iutfrp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_08_10_014511_UserTable', 1),
('2015_11_12_202800_News_Table', 2),
('2015_11_18_012734_Pregrado_Table', 2),
('2013_04_09_062329_create_revisions_table', 3),
('2015_12_02_010550_Pictures_Table', 4),
('2016_03_07_010550_Multimedia_Table', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multimedia`
--

CREATE TABLE IF NOT EXISTS `multimedia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) unsigned NOT NULL,
  `codename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(16384) COLLATE utf8_unicode_ci NOT NULL,
  `version` int(2) DEFAULT NULL,
  `imgPath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imgFoot` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pictures`
--

CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `imgPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `pictures`
--

INSERT INTO `pictures` (`id`, `type`, `position`, `imgPath`, `link`, `created_at`, `updated_at`, `remember_token`) VALUES
(4, 'carousel', '2', 'img/carousel/2.jpg', NULL, '2016-03-09 10:09:02', '2016-03-09 10:09:02', NULL),
(10, 'carousel', '1', 'img/carousel/1.jpg', 'http://google.com', '2016-04-04 06:42:04', '2016-04-04 06:42:04', NULL),
(11, 'carousel', '0', 'img/carousel/0.jpg', 'http://google.com', '2016-04-04 06:42:33', '2016-04-04 06:42:33', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregrado`
--

CREATE TABLE IF NOT EXISTS `pregrado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `contentPath` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `pregrado`
--

INSERT INTO `pregrado` (`id`, `name`, `contentPath`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'PNF Materiales Industriales', 'txt/pregrado/1.txt', '2016-04-03 13:10:43', '2016-04-03 13:10:43', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revisions`
--

CREATE TABLE IF NOT EXISTS `revisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `revisionable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8_unicode_ci,
  `new_value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=52 ;

--
-- Volcado de datos para la tabla `revisions`
--

INSERT INTO `revisions` (`id`, `revisionable_type`, `revisionable_id`, `user_id`, `key`, `old_value`, `new_value`, `created_at`, `updated_at`) VALUES
(1, 'News', 1, 1, 'created_at', NULL, '2015-11-29 18:29:53', '2015-11-29 22:59:53', '2015-11-29 22:59:53'),
(2, 'News', 2, 1, 'created_at', NULL, '2015-11-29 19:03:36', '2015-11-29 23:33:37', '2015-11-29 23:33:37'),
(3, 'News', 2, 1, 'deleted_at', NULL, '2015-11-29 23:33:41', '2015-11-30 04:03:41', '2015-11-30 04:03:41'),
(4, 'News', 3, 1, 'created_at', NULL, '2015-11-29 19:12:37', '2015-11-29 23:42:37', '2015-11-29 23:42:37'),
(5, 'News', 3, 1, 'deleted_at', NULL, '2015-11-29 23:42:50', '2015-11-30 04:12:50', '2015-11-30 04:12:50'),
(6, 'News', 4, 1, 'created_at', NULL, '2016-03-18 23:15:27', '2016-03-19 03:45:28', '2016-03-19 03:45:28'),
(7, 'News', 4, 1, 'deleted_at', NULL, '2016-03-19 04:19:41', '2016-03-19 08:49:41', '2016-03-19 08:49:41'),
(8, 'News', 5, 1, 'created_at', NULL, '2016-03-18 23:50:03', '2016-03-19 04:20:03', '2016-03-19 04:20:03'),
(9, 'News', 6, 1, 'created_at', NULL, '2016-03-18 23:52:06', '2016-03-19 04:22:06', '2016-03-19 04:22:06'),
(10, 'News', 7, 1, 'created_at', NULL, '2016-03-18 23:53:40', '2016-03-19 04:23:41', '2016-03-19 04:23:41'),
(11, 'News', 8, 1, 'created_at', NULL, '2016-03-21 00:43:58', '2016-03-21 05:13:59', '2016-03-21 05:13:59'),
(12, 'News', 9, 1, 'created_at', NULL, '2016-03-22 15:28:07', '2016-03-22 19:58:07', '2016-03-22 19:58:07'),
(13, 'News', 10, 1, 'created_at', NULL, '2016-03-22 15:42:05', '2016-03-22 20:12:05', '2016-03-22 20:12:05'),
(14, 'News', 11, 1, 'created_at', NULL, '2016-03-22 15:42:16', '2016-03-22 20:12:16', '2016-03-22 20:12:16'),
(15, 'News', 12, 1, 'created_at', NULL, '2016-03-22 15:42:30', '2016-03-22 20:12:30', '2016-03-22 20:12:30'),
(16, 'News', 13, 1, 'created_at', NULL, '2016-03-22 15:42:39', '2016-03-22 20:12:39', '2016-03-22 20:12:39'),
(17, 'News', 14, 1, 'created_at', NULL, '2016-03-22 15:44:15', '2016-03-22 20:14:15', '2016-03-22 20:14:15'),
(18, 'News', 15, 1, 'created_at', NULL, '2016-03-22 15:45:21', '2016-03-22 20:15:21', '2016-03-22 20:15:21'),
(19, 'News', 16, 1, 'created_at', NULL, '2016-03-22 15:46:43', '2016-03-22 20:16:43', '2016-03-22 20:16:43'),
(20, 'News', 17, 1, 'created_at', NULL, '2016-03-22 15:47:23', '2016-03-22 20:17:23', '2016-03-22 20:17:23'),
(21, 'News', 18, 1, 'created_at', NULL, '2016-03-22 15:48:02', '2016-03-22 20:18:03', '2016-03-22 20:18:03'),
(22, 'News', 19, 1, 'created_at', NULL, '2016-03-22 15:48:32', '2016-03-22 20:18:32', '2016-03-22 20:18:32'),
(23, 'News', 20, 1, 'created_at', NULL, '2016-03-22 15:50:48', '2016-03-22 20:20:48', '2016-03-22 20:20:48'),
(24, 'News', 21, 1, 'created_at', NULL, '2016-03-22 15:51:26', '2016-03-22 20:21:26', '2016-03-22 20:21:26'),
(25, 'News', 22, 1, 'created_at', NULL, '2016-03-22 15:53:41', '2016-03-22 20:23:41', '2016-03-22 20:23:41'),
(26, 'News', 22, 1, 'deleted_at', NULL, '2016-03-22 20:23:52', '2016-03-23 00:53:52', '2016-03-23 00:53:52'),
(27, 'News', 21, 1, 'deleted_at', NULL, '2016-03-22 20:23:54', '2016-03-23 00:53:54', '2016-03-23 00:53:54'),
(28, 'News', 23, 1, 'created_at', NULL, '2016-03-22 15:54:14', '2016-03-22 20:24:14', '2016-03-22 20:24:14'),
(29, 'News', 24, 1, 'created_at', NULL, '2016-03-22 22:58:56', '2016-03-23 03:28:57', '2016-03-23 03:28:57'),
(30, 'News', 25, 1, 'created_at', NULL, '2016-03-22 22:59:45', '2016-03-23 03:29:45', '2016-03-23 03:29:45'),
(31, 'News', 26, 1, 'created_at', NULL, '2016-03-22 23:45:36', '2016-03-23 04:15:36', '2016-03-23 04:15:36'),
(32, 'News', 26, 1, 'deleted_at', NULL, '2016-03-23 04:15:42', '2016-03-23 08:45:42', '2016-03-23 08:45:42'),
(33, 'News', 23, 1, 'deleted_at', NULL, '2016-03-23 04:37:33', '2016-03-23 09:07:33', '2016-03-23 09:07:33'),
(34, 'News', 19, 1, 'deleted_at', NULL, '2016-03-23 04:41:40', '2016-03-23 09:11:40', '2016-03-23 09:11:40'),
(35, 'News', 25, 1, 'deleted_at', NULL, '2016-03-23 04:42:29', '2016-03-23 09:12:29', '2016-03-23 09:12:29'),
(36, 'News', 24, 1, 'deleted_at', NULL, '2016-03-23 04:42:33', '2016-03-23 09:12:33', '2016-03-23 09:12:33'),
(37, 'News', 18, 1, 'deleted_at', NULL, '2016-03-23 04:42:36', '2016-03-23 09:12:36', '2016-03-23 09:12:36'),
(38, 'News', 15, 1, 'deleted_at', NULL, '2016-03-23 21:39:28', '2016-03-24 02:09:28', '2016-03-24 02:09:28'),
(39, 'News', 20, 1, 'deleted_at', NULL, '2016-04-04 02:13:11', '2016-04-04 06:43:11', '2016-04-04 06:43:11'),
(40, 'News', 17, 1, 'deleted_at', NULL, '2016-04-04 02:13:15', '2016-04-04 06:43:15', '2016-04-04 06:43:15'),
(41, 'News', 16, 1, 'deleted_at', NULL, '2016-04-04 02:13:19', '2016-04-04 06:43:19', '2016-04-04 06:43:19'),
(42, 'News', 14, 1, 'deleted_at', NULL, '2016-04-04 02:13:21', '2016-04-04 06:43:21', '2016-04-04 06:43:21'),
(43, 'News', 13, 1, 'deleted_at', NULL, '2016-04-04 02:13:23', '2016-04-04 06:43:23', '2016-04-04 06:43:23'),
(44, 'News', 12, 1, 'deleted_at', NULL, '2016-04-04 02:13:25', '2016-04-04 06:43:25', '2016-04-04 06:43:25'),
(45, 'News', 11, 1, 'deleted_at', NULL, '2016-04-04 02:13:27', '2016-04-04 06:43:27', '2016-04-04 06:43:27'),
(46, 'News', 10, 1, 'deleted_at', NULL, '2016-04-04 02:13:29', '2016-04-04 06:43:29', '2016-04-04 06:43:29'),
(47, 'News', 9, 1, 'deleted_at', NULL, '2016-04-04 02:13:31', '2016-04-04 06:43:31', '2016-04-04 06:43:31'),
(48, 'News', 8, 1, 'deleted_at', NULL, '2016-04-04 02:17:06', '2016-04-04 06:47:06', '2016-04-04 06:47:06'),
(49, 'News', 7, 1, 'deleted_at', NULL, '2016-04-04 02:17:10', '2016-04-04 06:47:10', '2016-04-04 06:47:10'),
(50, 'News', 6, 1, 'deleted_at', NULL, '2016-04-04 02:17:12', '2016-04-04 06:47:12', '2016-04-04 06:47:12'),
(51, 'News', 5, 1, 'deleted_at', NULL, '2016-04-04 02:17:15', '2016-04-04 06:47:15', '2016-04-04 06:47:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ci` int(11) NOT NULL,
  `user` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direction` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(29) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `ci`, `user`, `name`, `last_name`, `direction`, `email`, `password`, `type`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 244, 'Ora', 'Ora', 'Ora', 'Ora', 'Oraherrera7@gmail.com', '$2y$10$EfiLXuwqMWyyGOhvozPY.usetg3060/DheF9CScvIAb0nUAiA6sPG', 'Administrador de Publicacion', '0000-00-00 00:00:00', '2017-02-13 09:46:47', 'n5OSNTv7sNmKIvVeQynSLNxsG9z8GSV1OAqEdFng6oDrzFUKHcFvhen0Yjqe'),
(7, 26004129, 'adminTemp', 'Eduardo', NULL, NULL, 'eduardoalvara2@gmail.com', '$2y$10$EfiLXuwqMWyyGOhvozPY.usetg3060/DheF9CScvIAb0nUAiA6sPG', 'Administrador de Usuairos', '2016-04-05 02:38:49', '2017-02-13 22:31:14', 'tR5TVlEil8Yv3ydG8B0smmPaptm7ranfPaRDt9AHL4MK1nKjnThNCroL7jHv'),
(11, 24440522, 'OraMile', 'ORa', 'HErrera', 'LALALLLLLLLLLLLLLLLLLLLLLLLLLLLLksghjkjhgfdfghj', '', '$2y$10$Lit6FaHZK1gsBcgT40po0uOdJ9ZmFuJFJ44SaKQXW67l3.d7HX5n6', 'Mutimedia', '2017-02-12 07:51:10', '2017-02-13 22:23:04', NULL),
(13, 24440524, 'OriElena', 'Ori', 'Herrera', 'La quinta Los Teques', '', '$2y$10$vwAXAEZKmbq/Ri60BISPG.bkUZz5KNkRTrfooQ4StrtERJl83mePm', 'Pregrado', '2017-02-12 23:08:21', '2017-02-13 22:29:31', NULL),
(14, 12783782, 'Rocio', 'Rocio', 'Ramos', 'San Antonio Venezuela', '', '$2y$10$bNb.VX.HS68MI7rnIE.QFu/MdAdcxXT7mGPRJyI1vDQfLwhtADTCS', 'Administrador de Usuairos', '2017-02-13 22:13:42', '2017-02-13 22:13:42', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
