<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTable extends Migration {

	 /**
     * Run the migrations.
     *
     * @return void
     *
     * @since August 9, 2015.
     *
     * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
     */
    public function up()
    {
        Schema::create('User',function($table){
            $table->increments('id');

            $table->integer('ci');
           
            $table->enum('type',['Administrador de Usuairos','Administrador de Publicación','Noticias','Pregrado','Mutimedia']);

            $table->string('name', 11);

            $table->string('last_name', 11);

            $table->string('direction', 250);

            $table->string('user', 32)->unique;
            
            $table->string('email', 128);
            
            $table->string('password');
 
            //Adds created_at and updated_at columns
            $table->timestamps();

            //This column will be used to store a token for "remember me" sessions being maintained by your application
            $table->rememberToken();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('User');
    }

}
