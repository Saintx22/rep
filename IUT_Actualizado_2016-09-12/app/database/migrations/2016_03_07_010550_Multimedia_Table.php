<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultimediaTable extends Migration {

	public function up()
    { 

         Schema::create('Multimedia',function($table){

            $table->increments('id');
            $table->string('path');
            $table->string('ext');
            $table->string('description')->nullable();

            //Adds created_at and updated_at columns
            $table->timestamps();

            //This column will be used to store a token for "remember me" sessions being maintained by your application
            $table->rememberToken();

        });
    }

    public function down()
    {
         Schema::drop('Multimedia');
    }

}
