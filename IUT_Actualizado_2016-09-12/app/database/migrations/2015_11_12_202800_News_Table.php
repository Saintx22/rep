<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 

         Schema::create('News',function($table){
            $table->increments('id');
           
            $table->string('codename', 128);
            $table->string('title', 256);
            $table->string('content',16384);
            $table->string('imgPath')->nullable();
            $table->string('imgFoot',256)->nullable();
            $table->string('author', 128);

 
            //Adds created_at and updated_at columns
            $table->timestamps();

            //This column will be used to store a token for "remember me" sessions being maintained by your application
            $table->rememberToken();

            //Adds deleted_at columns
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('News');
    }

}
