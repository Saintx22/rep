<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PregradoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Pregrado',function($table){
            $table->increments('id');
           
            $table->string('name', 256);
            $table->string('contentPath', 256);
            

 
            //Adds created_at and updated_at columns
            $table->timestamps();

            //This column will be used to store a token for "remember me" sessions being maintained by your application
            $table->rememberToken();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Pregrado');
	}

}
