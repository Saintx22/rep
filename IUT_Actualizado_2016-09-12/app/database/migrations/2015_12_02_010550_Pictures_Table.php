<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PicturesTable extends Migration {

	public function up()
    { 

         Schema::create('Pictures',function($table){

            $table->increments('id');
            $table->string('type',20);
            $table->string('position',8);            
            $table->string('imgPath');
            $table->string('link')->nullable();

            //Adds created_at and updated_at columns
            $table->timestamps();

            //This column will be used to store a token for "remember me" sessions being maintained by your application
            $table->rememberToken();

        });
    }

    public function down()
    {
         Schema::drop('Pictures');
    }

}
