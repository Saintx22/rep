// app/database/seeds/UserTableSeeder.php

<?php


class UserTableSeeder extends Seeder
{

	public function run()
	{
	    DB::table('User')->delete();
	    User::create(array(
	        'ci'     => '9999999',
	        'user' => 'IUTFRP_ADMIN&123',
	        'email'    => 'iutfrp@gmail.com',
	        'password' => Hash::make('c7fje59j!'),
	    )); 
	}	

}