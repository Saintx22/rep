// app/database/seeds/UserTableSeeder.php

<?php


class PicturesTableSeeder extends Seeder
{

public function run()
	{

	    DB::table('pictures')->delete();

	    Pictures::create(array(
	        'type'     => 'carousel',
	        'position' => '1',
	        'imgPath'  => '/img/carousel/1.jpg',
	        'link' => '',
	    )); 

	    Pictures::create(array(
	        'type'     => 'carousel',
	        'position' => '2',
	        'imgPath'  => '/img/carousel/2.jpg',
	        'link' => '',
	    )); 

	    Pictures::create(array(
	        'type'     => 'carousel',
	        'position' => '3',
	        'imgPath'  => '/img/carousel/3.jpg',
	        'link' => '',
	    )); 

	    Pictures::create(array(
	        'type'     => 'carousel',
	        'position' => '4',
	        'imgPath'  => '/img/carousel/4.jpg',
	        'link' => '',
	    )); 
	    
	    Pictures::create(array(
	        'type'     => 'carousel',
	        'position' => '5',
	        'imgPath'  => '/img/carousel/5.jpg',
	        'link' => '',
	    )); 

	}

}