<?php

/**
*-----------------------------------------------------------
* Main Controller for News (Controlador Principal para Noticias).
*-----------------------------------------------------------
*
* @since June 13, 2015.
*
* @author Eduardo Alvarado <eduardoalvara2@gmail.com>
*
*/

class PregradoController extends BaseController {


    /**
    * Show View Add Pregrado (Mostrar Vista Añadir Pregrado).
    *-----------------------------------------------------------
    *
    * @since November 17, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function ShowAddPregrado(){

         return View::make('pregrado.addPregrado');
    }

    /**
    * Show View Admin Pregrado (Mostrar Vista Administrar Pregrado).
    *-----------------------------------------------------------
    *
    * @since November 17, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function ShowAdminPregrado(){

         return View::make('pregrado.adminPregrado');
    }

    /**
    * Add Pregrado (Añadir Pregrado).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View or Error.
    *
    */
    public function addPregrado(){

        return Pregrado::addPregrado();
      

    }

    /**
    * Show Pregrado (Mostrar Pregrado).
    *-----------------------------------------------------------
    *
    * @since Nov 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return View or Error.
    *
    */
    public function showPregrado($id){

        $pregrado= Pregrado::getPregrado($id);
      
        return View::make('pregrado.pregrado')->withPregrado($pregrado);

    }

    /**
    * Show view of all the news in the database (Mostrar Vista de todas las noticias de la base de datos).
    *-----------------------------------------------------------
    *
    * @since November 12, 2015.
    *
    * @author Ezequiel Ramirez <ezeram94@gmail.com> >:v
    *
    * @return View.
    *
    */
   /*
    public function ShowAllNews(){

        $newsArray = News::orderBy('id','desc')->get();
        return View::make('news.allNews')->with('newsArray',$newsArray);
    } */
    
    /**
    * Search Pregrados (Buscar Pregrados).
    *-----------------------------------------------------------
    *
    * @since Nov 17, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return View.
    *
    */

    public function searchPregrado(){

        $pregradoArray = Pregrado::all();
        return View::make('pregrado.searchPregrado')->with('pregradoArray',$pregradoArray);
    }

    /**
    * Delete Pregrado (Eliminar Pregrado).
    *-----------------------------------------------------------
    *
    * @since Nov 17, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return View.
    *
    */
    public function deletePregrado($id = null){

        $Pregrado = Pregrado::find($id);
        @unlink($Pregrado->contentPath);
        $Pregrado->delete();
        return Redirect::to('searchPregrado');
    }


     /**
    * Show Modify Pregrado (Mostrar Modificar Pregrado).
    *-----------------------------------------------------------
    *
    * @since Nov 17, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return View.
    *
    */
   

   /* MODIFICAR VIEJO 
   public function showModifyPregrado($id = null){

        $pregrado = Pregrado::find($id);

        return View::make('pregrado.modifyPregrado')->withPregrado($pregrado);
    }

    /**
    * Modify Pregrado (Modificar Pregrado).
    *-----------------------------------------------------------
    *
    * @since Nov 17, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return View.
    *
    */



   /* public function modifyPregrado($id = null){

        $pregrado=Pregrado::modifyPregrado($id);
      

        return Redirect::to('modifyPregrado/'.$id)->withPregrado($pregrado);
    }

*/
//MODIFICAR
    public function ShowChangePregrado($id){
         $pregrado=Pregrado::find($id);
         return View::make('pregrado/changePregrado')->with('pregrado',$pregrado);
    }
   
    public function changePregrado($id){
        return Pregrado::changePregrado($id);  
    }
    /*
     PDF of pregrado   
    */

    public function downloadPDF($pregrado,$URL){

        $view=View::make($URL)->with('pregrado',$pregrado);
        $pdf=App::make('dompdf');
        $pdf->loadHTML($view);
        return $pdf->download(trim(ucwords($pregrado->name)," ").'-pregradoIUTFRP.pdf');
    }

    public function findPDF($id){
        $pregrado = Pregrado::find($id);
        $URL='pregrado.PDF';
        return $this->downloadPDF($pregrado,$URL);
    }
}
