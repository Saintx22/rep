<?php

/**
*-----------------------------------------------------------
* Main Controller for News (Controlador Principal para Noticias).
*-----------------------------------------------------------
*
* @since June 13, 2015.
*
* @author Eduardo Alvarado <eduardoalvara2@gmail.com>
*
*/

class UserController extends BaseController {

    public function showLogin(){
        return View::make('admin.adminLogin');
    }

    /**
    * Authenticate User (Autenticar Usuario).
    *-----------------------------------------------------------
    *
    * @since August 9, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return redirect.
    *
    */
    public function doLogin(){

        Validator::extend('auth', function($attribute, $value, $parameters)
        {
            if (Auth::attempt($value)) {
                return true;
            }else{
                return false;
            }
        });
        $messages = array(
            "auth"           => "El Usuario o la Contraseña es incorrecta",
        );
        $data=array(
            'user'    => Input::get('user'), 
            'password' => Input::get('password'), 
            'user&password'     => array(
                                    'user'     => Input::get('user'),
                                    'password'  => Input::get('password')
                                    ),  
          );
        $rules = array(
            'user'    => 'required', 
            'password' => 'required' ,
            'user&password' => 'auth',

        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make($data, $rules,$messages);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {
            Session::put('key',User::where('user',Input::get('user'))->get(['type']));
            Session::put('username',User::where('user',Input::get('user'))->get(['user']));
             return Redirect::to('admin');
        }
    }

    public function doLogout(){
        Auth::logout(); // log the user out of our application
        return Redirect::to('/');
    }
	
	/**
    * Show all User.
    *-----------------------------------------------------------
    *
    * @since March 27, 2016.
    *
    * @author  Ezequiel Ramírez <ezeram94@gmail.com>
    *          Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return object.
    *
    */
	public function showUsers(){
        $Users =User::orderBy('ci','desc')->paginate('13');
        return View::make('user/all')->with('Users',$Users);
    }


    public function changePorfileShow(){
        $Users =Auth::user();
        return View::make('user/changePorfile')->with('Users',$Users);
      }
    public function showAddUser(){
        return View::make('user/newUser');
    }

    /**
    * Restore Password.
    *-----------------------------------------------------------
    *
    * @since March 23, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramírez <ezeram94@gmail.com>
    *
    * @return array.
    *
    */

    public function restorePassword($id = null){

        $return=User::restorePassword($id);

        if($return['success']){
            return Redirect::to('admin/users')->withMessages(array('Contraseña Restaurada es <strong>'.$return['pass'].'</strong>.'));
        }else{
            return Redirect::to('admin/users')->withErrors(array(''));
        }
        
    }

    /**
    * Add User.
    *-----------------------------------------------------------
    *
    * @since March 23, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramírez <ezeram94@gmail.com>
    *
    * @return array.
    *
    */
    public function addUser(){

        $data=array(
            'ci'                 => Input::get('ci'),
            'user'               => Input::get('user'),
            'type'               => Input::get('type'),
            'name'           
            => Input::get('name'),
            'last_name'          => Input::get('last_name'),
            'direction'          => Input::get('direction'),
            'email'              => Input::get('email'),
            'password'           => Input::get('password'),
            'passwordConfirm'    => Input::get('passwordConfirm'),
        );

        $result=User::add($data);

        if($result['success'])
        {
            return Redirect::to('admin/users')->withMessages($result['messages']);
        }else
        {
            return Redirect::to('admin/newUser')->withErrors($result['messages'])->withInput();
            
        }
    }


 /**
    * Add User.
    *-----------------------------------------------------------
    *
    * @since March 23, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramírez <ezeram94@gmail.com>
    *
    * @return array.
    *
    */
    public function changePorfile(){

        $data=array(
            'ci'                 => Input::get('ci'),
            'user'               => Input::get('user'),
            'name'               => Input::get('name'),
            'last_name'          => Input::get('last_name'),
            'direction'          => Input::get('direction'),
            'email'              => Input::get('email'),
        );

        $result=User::changePorfile($data);

        if($result['success'])
        {
            return Redirect::to('admin/user/changePorfile')->withMessages($result['messages']);
        }else
        {

            return Redirect::to('admin/user/changePorfile')->withErrors($result['messages'])->withInput();
            
        }
    }
    /**
    * Show Change Password View.
    *-----------------------------------------------------------
    *
    * @since March 27, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
      public function changePasswordShow(){

        return View::make('user.changePassword');
      }
    /**
    * Change Password.
    *-----------------------------------------------------------
    *
    * @since March 27, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
      public function changePassword(){

        
        $data=array(
            'oldPassword'        => Input::get('oldPassword'),
            'password'           => Input::get('password'),
            'passwordConfirm'    => Input::get('passwordConfirm'),
        );

        $result=User::changePassword($data);

        if($result['success'])
        {
            return Redirect::to('admin/user/changePassword')->withMessages($result['messages']);
        }else
        {
            return Redirect::to('admin/user/changePassword')->withErrors($result['messages'])->withInput();
            
        }
      }


    /**
    * Change Rol.
    *-----------------------------------------------------------
    *
    * @since March 27, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
      public function changeRol($id){
        $data=array('id'   => $id,
                    'type' => Input::get('type'),);

        $result=User::changeRol($data);
        $messageBag = array(
            'typeE' => 'error',
            'messages' => var_dump($result[0]),
            );

        switch ($result[0]) {
            case 'validator_fails':
                $messageBag['messages'] = $result['validator']->messages()->all();
                break;
             case 'delete_fails':
                $messageBag['messages'] = array('El rol de usuario no fue Modificado');
                break;
             case 'success':
                $messageBag['type'] = 'success';
                $messageBag['messages'] = array('¡El rol de Usuario fue Modificado con Éxito!');
                break;
            }
        if($result[0]=='success')
            return Redirect::to('admin/users')->with('messages',$messageBag['messages']);
        else
            return Redirect::to('admin/users')->with('errors',$messageBag['messages']);
    }








    /**
    * Delete User.
    *-----------------------------------------------------------
    *
    * @since April 3, 2016. (Remaked)
    *
    * @author  Ezequiel Ramírez <ezeram94@gmail.com>
    *          Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return object.
    *
    */
    public function deleteUser($id = null){

        $data=array('id' => $id);
        

        //DELETE
        $result = User::deleteUser($data);
        $messageBag = array(
            'type' => 'error',
            'messages' => var_dump($result[0]),
            );

        switch ($result[0]) {
            case 'validator_fails':
                $messageBag['messages'] = $result['validator']->messages()->all();
                break;
             case 'delete_fails':
                $messageBag['messages'] = array('El usuario no fue eliminado');
                break;
             case 'success':
                $messageBag['type'] = 'success';
                $messageBag['messages'] = array('¡Usuario eliminado con Éxito!');
                break;
            }
        if($result[0]=='success')
            return Redirect::to('admin/users')->with('messages',$messageBag['messages']);
        else
            return Redirect::to('admin/users')->with('errors',$messageBag['messages']);
    }
}