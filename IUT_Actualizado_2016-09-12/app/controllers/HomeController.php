<?php

class HomeController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    */

    /**
    * Muestar la vista de Foto Inicio
    *-----------------------------------------------------------
    *
    * @return View with data.
    *
    */
     public function showIndex()
    {

        //Obtener Noticias
        $newsArray = News::orderBy('id','desc')->take(4)->get();

        //Obtener url de la foto de alerta
        $photoIndexURL=@file_get_contents('txt/photoIndexURL.txt');
       	
        //Obtener Enlaces
       
        //Si está desactivado el carousel de la Institución, cargar el Carousel del Ministerio
        if(!file_exists('txt/carouselActivated'))
        {
            self::loadMinisteryCarousel();
        }
        return View::make('index')->with('newsArray',$newsArray)
                                  ->with('multimedia',Multimedia::orderBy('updated_at','desc')->get()->take(1))
                                  ->with('dir',Pictures::where('type','carousel')->get())
                                  ->with('photoIndexURL',$photoIndexURL);
    }

    /**
    * Muestar la vista de Foto Inicio Temporal
    *-----------------------------------------------------------
    * Este Inicio se muestra cuando la pagina está desactivada
    *
    *
    * @return View with data.
    *
    */
    public function showIndexDemo()
    {
        
        return View::make('index-demo');
    }

    public function showBienestar()
    {
        return View::make('bienestar');
    }

     public function showServices()
    {
        return View::make('services');
    }


    public function showCarreerInfo($carreer= null, $info = null)
    {
        $file= "txt/pregrado/inf-$carreer-$info.txt";
        $carreer = HomeController::carreerNames($carreer);
        $info = HomeController::infoNames($info);
        try
        {
            $content = str_replace("\n","<br>",File::get($file));

        }
        catch (Illuminate\Filesystem\FileNotFoundException $exception)
        {
            return View::make('pregrado.information')->with('carreer',$carreer)->with('info',$info)->with('content','ERROR: Informaci&oacute;n no existente.');
        }

        return View::make('pregrado.information')->with('carreer',$carreer)->with('info',$info)->with('content',$content);    
            
        
    }

    public function carreerNames($carreer){
        $aux="";
        switch($carreer)
        {
            case 'electronica': $aux="Electr&oacute;nica";break;
            case '': $aux="";break;
        }
        return $aux;
    }

    public function infoNames($info){
        $aux="";
        switch($info)
        {
            case 'ingeniero': $aux="Ingeniero";break;
            case 'tsu':         $aux="T&eacute;cnico Superior Universitario";break;
            case 'asistente':   $aux="Asistente";break;
            case '': $aux="";break;
        }
        return $aux;
    }

    /**
    * Muestra Información de los Creadores
    *-----------------------------------------------------------
    *
    * @since March 27, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return View.
    *
    */
     public function aboutUs(){

        return View::make('aboutUs');
    }

    /**
    * Carga el Carousel del Ministerio si es necesario hacerlo.
    *-----------------------------------------------------------
    *
    * @since March 31, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return void.
    *
    */
     public function loadMinisteryCarousel(){
        // - Proceso De Recarga Del Carousel del Ministerio

        //Chequear conexion a internet
        if(@fsockopen('www.google.com',80))
        {
            // Crea un archivo con la fecha de hoy
            $file = 'txt/externalCarousel/'.date('Y-m-d',strtotime(date_default_timezone_get()));

            //Si este archivo ya fue creado, significa también que el Carousel fue ya cargado el día de hoy.
            if (!file_exists($file)) {
                //Si no fue creado, entonces se debe cargar hoy.

                //Eliminamos todos los archivos que estén dentro de la carpeta externalCarousel para limpiar.
                $files = glob('txt/externalCarousel/*',GLOB_MARK);
                foreach ($files as $key) {
                   @unlink($key);
                }

                // Cargamos la pagina del ministerio
                $url= "http://www.mppeuct.gob.ve/";
                $content = @file_get_contents($url);
                if($content)
                {
                    $f=fopen('ministerio.html','w') or die('ERROR');
                    fwrite($f, $content);
                    fclose($f);
                }

                //Creamos el archivo con la fecha de hoy para conocer que hoy ya fue cargado el Carousel.
                $f=fopen($file,'w') or die('ERROR');
                fclose($f);
            }
        }
    } 

    /**
    * Función Estándar que Carga las Imágenes Con Hipervinculos.
    *-----------------------------------------------------------
    *
    * @since April 1, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array or false.
    *
    */
    public static function loadImageWithLink($path){
        if( $path)
        {
            $links = array();

            $dir= scandir($path) ;
            $supported_images = array('gif','jpg','jpeg','png','bmp');
            foreach ($dir as $file)
            {   
                $extension = @pathinfo($file)['extension'];
                //Si los archivos en el link poseen las extensiones definidas es "$supported_images"
                if(in_array(strtolower($extension),$supported_images))
                {   
                    array_push($links, array(
                                        'img'    => URL::to($path.$file), 
                                        'path' => $path.$file,
                                        'url'    => base64_decode(basename($file,'.'.$extension)),
                                        ));
                }
            }
            return $links;
        }
        return false;
    }

    /**
    * Carga las Imágenes de los Enlaces
    *-----------------------------------------------------------
    *
    * @since April 1, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array or false.
    *
    */
     public static function loadLinks(){
        
        $path= 'img/events/' ;

        return self::loadImageWithLink($path);
    }

    /**
    * Carga las Imágenes de los Relacionados
    *-----------------------------------------------------------
    *
    * @since April 3, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array or false.
    *
    */
     public static function loadRelated(){
        
        $path= 'img/related/';

        return self::loadImageWithLink($path);
    }
}
