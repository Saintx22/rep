<?php

/**
*-----------------------------------------------------------
* Main Controller for News (Controlador Principal para Noticias).
*-----------------------------------------------------------
*
* @since June 13, 2015.
*
* @author Eduardo Alvarado <eduardoalvara2@gmail.com>
*
*/

class NewsController extends BaseController {


    /**
    * Show View Add New (Mostrar Vista Añadir Noticia).
    *-----------------------------------------------------------
    *
    * @since June 13, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function ShowAddNew(){

         return View::make('news.addNews');
    }
/**
    * Show View Change New (Mostrar Vista Modificar Noticia).
    *-----------------------------------------------------------
    *
    * @since June 13, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function ShowChangeNew($id){
         $news=News::find($id);
         return View::make('news/changeNews')->with('news',$news);
    }
    /**
    * Change News (Modificar Noticia).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View or Error.
    *
    */
    public function ChangeNews($id){
        return News::ChangeNews($id);  

    }
    /**
    * Show View Admin News (Mostrar Vista Administrar Vistas).
    *-----------------------------------------------------------
    *
    * @since June 13, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function ShowAdminNews(){

         // return View::make('news.adminNews');
        return NewsController::searchNews();
         
    }

    /**
    * Add News (Añadir Noticia).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View or Error.
    *
    */
    public function addNews(){


        return News::addNews();  

    }

    /**
    * Show News (Mostrar Noticia).
    *-----------------------------------------------------------
    *
    * @since Nov 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return View or Error.
    *
    */
    public function ShowNews($id){

        $news= News::getNews($id);
      
        return View::make('news.news')->withNews($news);

    }
    /**
    * Show News (Mostrar Noticia).
    *-----------------------------------------------------------
    *
    * @since Nov 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return View or Error.
    *
    */
    public function ShowNewsVersion($codename,$version){

       $newsV = DB::table('news')
                    ->where('codename', $codename)
                    ->where('version', $version)
                    ->get();
       $news= News::getNews($newsV[0]->id);
        return View::make('news.news')->withNews($news);

    }
    /**
    * Show view of all the versions in the database (Mostrar Vista de todas las noticias de la base de datos).
    *-----------------------------------------------------------
    *
    * @since November 12, 2015.
    *
    * @author Ezequiel Ramirez <ezeram94@gmail.com> >:v
    *
    * @return View
    *
    */

    public function ShowAllVersion($id){

        $news = News::where('id', $id)
                    ->orWhere('name', 'John')
                    ->get();

        $newsArray = News::where('codename', DB::raw($news));
        return View::make('news.allNews')->with('newsArray',$newsArray);
    }
    /**
    * Show view of all the news in the database (Mostrar Vista de todas las noticias de la base de datos).
    *-----------------------------------------------------------
    *
    * @since November 12, 2015.
    *
    * @author Ezequiel Ramirez <ezeram94@gmail.com> >:v
    *
    * @return View.
    *
    */

    public function ShowAllNews(){

        $newsAr = DB::table('news')
                            ->orderBy('version','desc')
                            ->toSql();

        $newsArr =DB::table('news')
                           ->from(\DB::raw("($newsAr) news"))
                           ->groupBy('codename') 
                           ->toSql();

        $newsArray = News::from(\DB::raw("($newsArr) news"))
                           ->orderBy('updated_at','desc')
                           ->paginate('4');
        return View::make('news.allNews')->with('newsArray',$newsArray);
    }
    
    /**
    * Get the news (Obtiene las noticias)
    *-----------------------------------------------------------
    *
    * @since November 29, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return Object.
    *
    */

    public function searchNews(){

        $newsAr = DB::table('news')
                            ->orderBy('version','desc')
                            ->toSql();

        $newsArr =DB::table('news')
                           ->from(\DB::raw("($newsAr) news"))
                           ->groupBy('codename') 
                           ->toSql();

        $newsArray = News::from(\DB::raw("($newsArr) news"))
                           ->orderBy('updated_at','desc')
                           ->paginate('4');
        return View::make('news.searchNews')->with('newsArray',$newsArray);
    }

    public function deleteNews($id = null){

        if(News::DeleteNews($id))
        {
            return Redirect::to('searchNews')->withMessages(array('¡Noticia Eliminada Exitosamente!.'));
        }else{
            return Redirect::to('searchNews')->withErrors(array('Ocurrió un error al intentar eliminar la noticia...'));
        }
        
    }


}
