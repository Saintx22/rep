<?php

class CarouselController extends BaseController {

		
	public function showCarouselEdit()
	{

		$array = Pictures::where('type','=','carousel')->get();
		return View::make('carousel.carouselEdit')->with('array',$array);
	}


	public function showNewPhoto()
	{
		return View::make('carousel.newPicture');
	}

	public function addNewPhoto()
	{
        $attributes = array(
            'position' => 'Nombre',
            );

		$rules = array(
            'position' => 'required|unique:pictures,position',
	        'img'    => 'required', 
            'link'  => 'url',
			);

        $validator = Validator::make(Input::all(), $rules,array(),$attributes);

        if ($validator->fails()) {
            return Redirect::to('admin/newPicture')->withErrors($validator)->withInput();
        } else {
        	$Pictures = new Pictures;
        	$Pictures->position =  Input::get('position');
        	$Pictures->type =  'carousel';
        	$Pictures->imgPath = 'img/carousel/'.Input::get('position').'.jpg';
        	$Pictures->link = Input::get('link');
            $Pictures->save();
        	Input::file('img')->move('img/carousel', Input::get('position').'.jpg');
            //$imgP=Pictures::where('position','=', "'img/carousel/".Input::get('position').".jpg'")->get('id');
            $imgP=DB::table('pictures')->where('imgPath', "img/carousel/".Input::get('position').".jpg")->pluck('id');
            if (Input::get('date')!="") {
                DB::unprepared("
                    CREATE EVENT DeleteImg".Input::get('position')."
                    ON SCHEDULE AT '".Input::get('date')." 00:00'
                    DO DELETE FROM `pictures` WHERE `id`=".$imgP."
                   ");
            }

            return Redirect::to('admin/carouselEdit');
        }

	}

	public function deletePhoto($filename = null)
	{

		$Pictures = Pictures::where('position','=', $filename)->delete();
		@unlink('img/carousel/'.$filename.'.jpg');
		return Redirect::to('admin/carouselEdit');

	}

	 /**
    * Activar Desactivar Carousel.
    *-----------------------------------------------------------
    *
    * @since March 9, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return Return
    *
    */
    public function switchCarousel(){

        $switch = Input::get('carouselSwitch');

        if($switch == '1')
        {
            $file= fopen('txt/carouselActivated', 'w');
            fclose($file);
        }else{
            @unlink('txt/carouselActivated');
        }

        return Redirect::to('admin/carouselEdit');
    }
}
