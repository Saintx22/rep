<?php

class AdminController extends BaseController {

    /**
    * Muestar el menú Administrador
    *-----------------------------------------------------------
    *
    * @since UNKNOWN
    *        EDITED: March 31, 2016
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function showAdminMenu()
    {
        return View::make('admin.admin');
    }

    /**
    * Muestar la vista de Cambiar Encabezado
    *-----------------------------------------------------------
    *
    * @since UNKNOWN
    *        EDITED: March 31, 2016
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function showTopBarChange(){
    
        return View::make('admin.topBarChange');
    }

     /**
    * Cambiar Encabezado (FUNCION NO OPTIMIZADA, CÓDIGO ANTIGUO)
    *-----------------------------------------------------------
    *
    * @since UNKNOWN
    *        EDITED: March 31, 2016
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return Redirect.
    *
    */
    public function topBarChange(){
        
        $maxh=50;
        $ext='png';
        $dir='img';
        
        
        $file = Input::file('img-1');
        $name=  'img-1';
        $oname=  'img-1_o';
        $maxw=500;
        if($file)
        {
            
            $oext=$file->getClientOriginalExtension();  
            $file->move($dir,$oname.'.'.$oext);
        }else{
            $oext='png';
        }
        AdminController::resized($dir,$oname,$name,$oext, $ext, $maxw,$maxh,false);
        
        
        $file = Input::file('img-2');
        $name=  'img-2';
        $oname=  'img-2_o';
        $maxw=150;
        if($file)
        {
            $oext=$file->getClientOriginalExtension();  
            $file->move($dir,$oname.'.'.$oext);
        }else{
            $oext='png';
        }
        AdminController::resized($dir,$oname,$name,$oext, $ext, $maxw,$maxh,true);
        
            
        $file = Input::file('img-3');
        $name=  'img-3';
        $oname=  'img-3_o';
        $maxw=150;
        if($file)
        {
            $oext=$file->getClientOriginalExtension();  
            $file->move($dir,$oname.'.'.$oext);
        }else{
            $oext='png';
        }
        AdminController::resized($dir,$oname,$name,$oext, $ext, $maxw,$maxh,true);
        
        return Redirect::to('admin/topBarChange');
    }


    //(FUNCION NO OPTIMIZADA, CÓDIGO ANTIGUO)
    function resized($dir,$oname,$name,$oext, $ext,$maxw,$maxh,$center=false)
    {
        
        
        $ofilename=$dir.'/'.$oname.'.'.$oext;
        $filename=$dir.'/'.$name.'.'.$ext;
        
        switch(strtoupper($oext))
        {
            case('JPG'):
            case('JPEG'): $im=@imagecreatefromjpeg($ofilename); break;
            case('PNG'): $im=@imagecreatefrompng($ofilename); break;
            case('GIF'): $im=@imagecreatefromgif($ofilename); break;
            default: return 'Formato no reconocido.';
        }
        
        if($im)
        {
            
            
            $array = getimagesize($ofilename);
            File::delete($ofilename);
            imagepng($im,$dir.'/'.$oname.'.png');
            $ow=$array[0];
            $oh=$array[1];
            
            $w=$ow;
            $h=$oh;
            
            if($w>$maxw)
            {
                $h=($h*$maxw)/$w;
                $w=$maxw;
            }
            if($h>$maxh)
            {
                $w=($w*$maxh)/$h;
                $h=$maxh;
            }
            
            if(($w<>$maxw) || ($h<>$maxh))
            {
                $imC=imagecreatetruecolor($maxw,$maxh);
                
                $white = imagecolorallocate($imC, 255, 255, 255);
                
                imagefill($imC,0,0,$white);
                $x=0;
                if($center)
                {
                    $x=($maxw-$w)/2;
                }
                
                imagecopyresampled($imC, $im, $x, 0, 0, 0, $w, $h, $ow, $oh);
                
                imagepng($imC,$filename);
                imagedestroy($imC);
                imagedestroy($im);
            }
        }
    }
    // END REZIZED FUNCTION
    

    /**
    * Muestar la vista de Foto Inicio
    *-----------------------------------------------------------
    *
    * @since March 8, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function showIndexPhoto(){
        $photoIndexURL=@file_get_contents('txt/photoIndexURL.txt');
        return View::make('admin.indexPhoto')->with('photoIndexURL',$photoIndexURL);
    }

    //--- indexPhoto
    /**
    * Actualiza la Imagen
    *-----------------------------------------------------------
    *
    * @since March 8, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function indexPhotoUpload(){


        $response= Img::indexPhotoUpload('img');

        return Redirect::to('admin/indexPhoto');
    }


    /**
    * Actualiza la configuraciíon de si se mostrará o no la Imagen de Alerta
    *-----------------------------------------------------------
    *
    * @since March 8, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public static function indexPhotoSetDisplay(){

        $show = Input::get('mostrar');

         Img::indexPhotoSetDisplay($show);

        return Redirect::to('admin/indexPhoto');


    }

    /**
    * Actualiza la URL de la imágen de alerta
    *-----------------------------------------------------------
    *
    * @since March 30, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return Redirect.
    *
    */
    public static function indexPhotoSetURL(){

        $url = Input::get('url');

        if(Img::indexPhotoSetURL($url)){
            $message= array('type'=>'success','content'=>'¡Actualizado!.');
        }else{
            $message= array('type'=>'error','content'=>'Introduzca un URL válido. Ej: "http://google.com"');
        }
        return Redirect::to('admin/indexPhoto')->withMessage($message)->withInput();;
    }

    /**
    * Remueve URL de la imágen de Alerta
    *-----------------------------------------------------------
    *
    * @since March 30, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return Redirect.
    *
    */
    public static function indexPhotoUnsetURL(){

        if(Img::indexPhotoUnsetURL()){
            $message= array('type'=>'success','content'=>'Hipervínculo removido.');
        }else{
            $message= array('type'=>'error','content'=>'No se pudo desasignar el hipervínculo.');
        }
        return Redirect::to('admin/indexPhoto')->withMessage($message);;
    }


    /**
    * Activa o Desactiva la página
    *-----------------------------------------------------------
    *
    * @since March 29, 2016.
    *
    * @author Ezequiel Ramirez <ezeram94@gmail.com> :v
    *
    * @return Redirect.
    *
    */
    public function available(){
        $file = 'txt/available';
        if (file_exists($file)){
            @unlink($file);
        }
        else{
            fopen($file, 'a');
        }
        return Redirect::to('admin');
    }
   
     /**
    * Muestar la vista de Foto Inicio
    *-----------------------------------------------------------
    *
    * @since March 31, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function showLinks(){
        $links= HomeController::loadLinks();
       
        return View::make('admin.links')->with('links',$links);
    }

     /**
    * Muestar la vista de Foto Inicio
    *-----------------------------------------------------------
    *
    * @since March 31, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return View.
    *
    */
    public function showRelated(){
        $allRelated= HomeController::loadRelated();
       
        return View::make('admin.related')->with('allRelated',$allRelated);
    }

    /**
    * Agrega una imagen con hipervínculo
    *-----------------------------------------------------------
    *
    * @since March 31, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
    public function addImageWithLink($path){
       

        $messageBag = array(
            'type' => 'danger',
            'messages' => array('danger'),
            );

        $data= array(
            'img'=>Input::file('img'),
            'url'=>Input::get('url'),
            );

        //Model
        $result= Img::addImageWithLink($data,$path);

        switch ($result[0]) {
            case 'error_validator':
                $messageBag['messages'] = $result['validator']->all();
                break;
            case 'error_move':   
                // $messageBag['messages'] = array($result[1]); // Exception Error
                $messageBag['messages'] = array('Ocurrió un error al intentar guardar la Imagen');
                break;
            case 'success':   
                $messageBag['type'] = 'success';
                $messageBag['messages'] = array('¡Imagen Agregada con Éxito!');
                break;
        }

        return $messageBag;
    }

    /**
    * Agrega una imagen "Enlace"
    *-----------------------------------------------------------
    *
    * @since April 3, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return view with array.
    *
    */
    public function addLink(){

        $path='img/events/';

        $messageBag = self::addImageWithLink($path);

        return Redirect::to('admin/links')->with('messageBag',$messageBag);
    }

    /**
    * Agrega una imagen "Relacionada"
    *-----------------------------------------------------------
    *
    * @since April 3, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return view with array.
    *
    */
    public function addRelated(){

        $path='img/related/';

        $messageBag = self::addImageWithLink($path);

        return Redirect::to('admin/related')->with('messageBag',$messageBag);
    }

    /**
    * Elimina una imagen con hipervínculo
    *-----------------------------------------------------------
    *
    * @since April 1, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return view with array.
    *
    */
    public function deleteImageWithLink($path){

        $messageBag = array(
            'type' => 'danger',
            'messages' => array('danger'),
            );

        //DELETE
        File::delete($path);

        if(!File::exists($path))
        {
            $messageBag['type'] = 'success';
            $messageBag['messages'] = array('¡Imagen Eliminada con Éxito!');
        }else{
            $messageBag['messages'] = array('Imagen no fue eliminada');           
        }

       return $messageBag;
    }

    /**
    * Elimina una imagen "Enlace"
    *-----------------------------------------------------------
    *
    * @since April 3, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return view with array.
    *
    */
    public function deleteLink(){

       $path= Input::get('path');

       $messageBag= self::deleteImageWithLink($path);

       return Redirect::to('admin/links')->with('messageBag',$messageBag);
    }

    /**
    * Elimina una imagen "Relacionada"
    *-----------------------------------------------------------
    *
    * @since April 3, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return view with array.
    *
    */
    public function deleteRelated(){

       $path= Input::get('path');

       $messageBag= self::deleteImageWithLink($path);

       return Redirect::to('admin/related')->with('messageBag',$messageBag);
    }

    /**
    * Muestra la vista para editar los archivos HTML (Bienestar, Servicios, IUT, Inicio)
    *-----------------------------------------------------------
    *
    * @since April 4, 2016.
    *
    * @author Ezequiel Ramirez <ezeram94@gmail.com>
    *
    * @return view with array.
    *
    */
    public function showAllHtml(){

		$path  = '../app/views/html' ;

		$dir = scandir($path); array_shift($dir);array_shift($dir);

		return View::make('admin/allHtml')->with('dir',$dir);
    }

     /**
    * Muestra la vista para editar un unico archivo HTML
    *-----------------------------------------------------------
    *
    * @since April 4, 2016.
    *
    * @author Ezequiel Ramirez <ezeram94@gmail.com>
    *
    * @return view with array.
    *
    */
    public function showEditHtml($name){
		
		return View::make('admin/editHtml')->with('name',$name);
		
    }

     /**
    * Actualiza la informacion dentro del HTML seleccionado
    *-----------------------------------------------------------
    *
    * @since April 4, 2016.
    *
    * @author Ezequiel Ramirez <ezeram94@gmail.com>
    *
    * @return view with array.
    *
    */

    public function EditHtml($name){

		$path  = '../app/views/html' ;

		$file = fopen($path.'/'.$name, 'w+');

		fputs($file,	 Input::get('content'));


				
		return Redirect::to('admin/html/'.$name)->with('name',$name);
		
    }

}
