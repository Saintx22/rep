<?php

class MultimediaController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | 
    |--------------------------------------------------------------------------
    |
    |
    |   
    |
    */

    public function showMultimedia(){
        return View::make('galery')->with('multimedia',Multimedia::orderBy('id','desc')->paginate(4));
    }

    public function manageMultimedia(){
        return View::make('multimedia/all')->with('multimedia',Multimedia::orderBy('id','desc')->paginate(4));

    }
    public function addMultimedia(){
            
            $multimedia = new Multimedia();
            $multimedia->validate(Input::file('archivo'),Input::get('description'));
            return Redirect::to('admin/multimedia');
    }


    public function downloadMultimedia($id){
        $multimedia = Multimedia::find($id);
        $path=$multimedia->path;
        $fileName = substr($path,11);
        $file=file($path);
        $file2=implode("",$file);
        header("Content-Type:application/octet-stream");
        header("content-Disposition: attachment; filename=$fileName");
        echo $file2;
    }
    
    public function deleteMultimedia($id){

        $multimedia = Multimedia::find($id);
        
        @unlink($multimedia->path);
        $multimedia->delete();
        return Redirect::to('admin/multimedia');
    }

    public function changeMultimedia($id){

        @unlink($multimedia->path);
        $multimedia->validateChange($id,Input::file('archivo'),Input::get('description'));
        return Redirect::to('admin/multimedia');
    }

}

