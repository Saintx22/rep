@extends('layout')

@section('title') Multimedia @stop

@section('content')


	<div class="container">
		<h3>Multimedia</h3>
		<hr>
		{{$multimedia->links()}}

		@foreach ($multimedia as $key)
		<div class="row">	
			<div class="col-sm-8">
				{{--*/$host  = $_SERVER['HTTP_HOST'];
				$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
				$base = substr("http://". $host . $uri,0,-9).$key->path;/*--}}
				@if (!($key->ext == 'webm' || $key->ext == 'mp4' || $key->ext == 'pdf'))
				{{HTML::image($key->path,'',array('class' => 'img-responsive','style' => 'max-width:100%;height:auto;'))}}
				<a href="{{URL::to('download/multimedia/'.$key->id)}}"><button class="btn btn-block btn-primary btn-xs">Descargar Imagen</button></a>
				@elseif (($key->ext == 'pdf'))
				<embed src="{{$base}}" width='750' height='505'>
				<a href="{{URL::to('download/multimedia/'.$key->id)}}"><button class="btn btn-block btn-primary btn-xs">Descargar PDF</button></a>
				@else
				<video controls class="img-responsive border" style="max-width:100%;height:auto;">
                <source src="{{$base}}" type="video/mp4"></source>
           		</video>
				<a href="{{URL::to('download/multimedia/'.$key->id)}}"><button class="btn btn-block btn-primary btn-xs">Descargar Video</button></a>
				@endif
				<h5>{{$key->description}}</h5>

			</div>
			
				
			
		</div>

		<hr>
		@endforeach
		{{$multimedia->links()}}
	</div>


	
@stop