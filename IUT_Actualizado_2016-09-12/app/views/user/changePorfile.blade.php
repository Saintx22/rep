@extends('user/layoutUsers')
@section('content')
        
    <h1> Modificar Perfil </h1>
     
    {{ Form::open(array('class'=>'form-horizontal')) }}

        <!-- CEDULA -->
        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('ci'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('ci', 'Cédula: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6">
                {{ Form::text('ci', Input::old('ci'), array('class' =>'form-control', 'maxlength' => 9,'placeholder' => $Users->ci)) }}
            </div>
        </div>

        <!-- NOMBRE DE USUARIO -->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('user'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('user', 'Nombre de Usuario: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 4; Max 24 caracteres">
                {{ Form::text('user', Input::old('user'), array('class' =>'form-control', 'maxlength' => 32,'placeholder' => $Users->user)) }}
            </div>
        </div>

        <!-- NOMBRE-->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('name'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('name', 'Nombre: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 3; Max 11 caracteres">
                {{ Form::text('name', Input::old('name'), array('class' =>'form-control', 'maxlength' => 11,'placeholder' => $Users->name)) }}
            </div>
        </div>

        <!-- APELLIDO-->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('last_name'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('last_name', 'Apellido: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6" data-toggle="tooltip" data-placement="top" title="Min 2; Max 11 caracteres">
                {{ Form::text('last_name', Input::old('last_name'), array('class' =>'form-control', 'maxlength' => 11,'placeholder' => $Users->last_name)) }}
            </div>
        </div>

        <!-- DIRECCION-->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('direction'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('direction', 'Dirección: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 10; Max 250 caracteres">
                {{ Form::text('direction', Input::old('direction'), array('class' =>'form-control', 'maxlength' => 250,'placeholder' => $Users->direction)) }}
            </div>
        </div>

        <!-- CORREO-->

         <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('email'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('email', 'Correo: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Max 128 caracteres">
                {{ Form::text('email', Input::old('email'), array('class' =>'form-control','placeholder' => $Users->email)) }}
            </div>
        </div>

        
            
            {{ Form::submit('Modificar', array('class' => 'btn btn-primary col-xs-offset-3')) }}
            <a href="{{URL::to('admin')}}"><button type="button" class="btn btn-warning" data-dismiss="modal" >Cancelar</button></a>

       
    {{ Form::close() }}
    <br>
    <!-- if there are errors, show them here -->
    
     @if(@Session::get('errors'))
        @foreach(Session::get('errors')->all() as $message)
            <div class="alert alert-danger" role="alert">

                {{$message}}
           
            </div>
        @endforeach
    @endif
@stop