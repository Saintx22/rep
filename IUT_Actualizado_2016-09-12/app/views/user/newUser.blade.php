@extends('user/layoutUsers')
@section('content')
        
    <h1> A&ntilde;adir Usuario </h1>
     
    {{ Form::open(array('class'=>'form-horizontal')) }}

        <!-- CEDULA -->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('ci'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('ci', 'Cédula: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6">
                {{ Form::text('ci', Input::old('ci'), array('class' =>'form-control', 'maxlength' => 9)) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>
        </div>

        <!-- NOMBRE DE USUARIO -->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('user'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('user', 'Nombre de Usuario: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 4; Max 24 caracteres">
                {{ Form::text('user', Input::old('user'), array('class' =>'form-control', 'maxlength' => 32)) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>
        </div>

        <!-- ROLES DE USUARIO -->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('type'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('type', 'Permiso de gestión: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6">
                {{ Form::select('type', ['Administrador de Usuairos'=>'Administrador de Usuairos',
                                         'Administrador de Publicacion'=>'Administrador de Publicación',
                                         'Noticias'=>'Noticias',
                                         'Pregrado'=>'Pregrado',
                                         'Mutimedia'=>'Mutimedia'],
                                         'Noticias',['class' =>'form-control']) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>
        </div>

        <!-- NOMBRE-->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('name'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('name', 'Nombre: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 3; Max 11 caracteres">
                {{ Form::text('name', Input::old('name'), array('class' =>'form-control', 'maxlength' => 11)) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>
        </div>

        <!-- APELLIDO-->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('last_name'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('last_name', 'Apellido: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 2; Max 11 caracteres">
                {{ Form::text('last_name', Input::old('last_name'), array('class' =>'form-control', 'maxlength' => 11)) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>
        </div>

        <!-- DIRECCION-->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('direction'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('direction', 'Dirección: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6" data-toggle="tooltip" data-placement="top" title="Min 10; Max 250 caracteres">
                {{ Form::text('direction', Input::old('direction'), array('class' =>'form-control', 'maxlength' => 250)) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>
        </div>

        <!-- CORREO-->

         <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('email'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('email', 'Correo: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Max 128 caracteres">
                {{ Form::text('email', Input::old('email'), array('class' =>'form-control')) }}
            </div>
        </div>

        <!-- CONTRASEÑA-->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('password'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('password', 'Contrase&ntilde;a: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 8; Max 16 caracteres">
                {{ Form::password('password', array('class' =>'form-control', 'maxlength' => 16)) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>
        </div>

        <!-- CONFIRMAR CONTRASEÑA-->

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('passwordConfirm'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('passwordConfirm', 'Confirmar Contrase&ntilde;a: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 8; Max 16 caracteres">
                {{ Form::password('passwordConfirm', array('class' =>'form-control', 'maxlength' => 16))}}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>
        </div>

        
            
            {{ Form::submit('A&ntilde;adir', array('class' => 'btn btn-primary col-xs-offset-3')) }}
            <a href="{{URL::to('admin/users')}}"><button type="button" class="btn btn-warning" data-dismiss="modal" >Cancelar</button></a>

    {{ Form::close() }}
    <br>
    <!-- if there are errors, show them here -->
    
     @if(@Session::get('errors'))
        @foreach(Session::get('errors')->all() as $message)
            <div class="alert alert-danger" role="alert">

                {{$message}}
           
            </div>
        @endforeach
    @endif
@stop
@section('js')

<script type="text/javascript" src="../../../public_html/js/bootstap.min.js"</script>
<script type="text/javascript" src="../../../public_html/js/jquery-1.11.1.min.js"</script>
@stop