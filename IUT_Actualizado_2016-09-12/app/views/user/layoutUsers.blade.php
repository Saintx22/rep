@extends('admin.layoutAdmin')
@if(@Session::get('key')=='[{"type":"Administrador de Usuairos"}]')
@section('title')Gestionar Usuarios @stop

@section('options')
    <a class="list-group-item list-group-item-success" href="{{URL::to('admin/newUser')}}"><i class="fa fa-user-plus"></i>&nbsp; A&ntilde;adir Usuario</a>
    <a class="list-group-item list-group-item-info" href="{{URL::to('admin/users')}}"><i class="fa fa-users"></i>&nbsp; Ver todos los Usuarios </a>    
    <hr>
@stop
@endif
