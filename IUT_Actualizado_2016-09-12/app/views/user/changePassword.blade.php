@extends('user/layoutUsers')
@section('content')
        
    <h1> Cambiar Contraseña </h1>
     
    {{ Form::open(array('class'=>'form-horizontal')) }}

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('oldPassword'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('oldPassword', 'Contrase&ntilde;a Actual: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6">
                {{ Form::password('oldPassword', array('class' =>'form-control')) }}
            </div>
        </div>

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('password'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('password', 'Nueva Contrase&ntilde;a: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 8; Max 16 caracteres">
                {{ Form::password('password', array('class' =>'form-control', 'maxlength' => 16)) }}
            </div>
        </div>

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('passwordConfirm'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('passwordConfirm', 'Confirmar la Nueva Contrase&ntilde;a: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6"  data-toggle="tooltip" data-placement="top" title="Min 8; Max 16 caracteres">
                {{ Form::password('passwordConfirm', array('class' =>'form-control', 'maxlength' => 16)) }}
            </div>
        </div>

            {{ Form::submit('A&ntilde;adir', array('class' => 'btn btn-primary col-xs-offset-3')) }}
            <a href="{{URL::to('admin')}}"><button type="button" class="btn btn-warning" data-dismiss="modal" >Cancelar</button></a>

       
    {{ Form::close() }}
    <br>
    <!-- if there are errors, show them here -->
     @if(@Session::get('errors'))
        @foreach(Session::get('errors')->all() as $message)
            <div class="alert alert-danger" role="alert">
               
                {{$message}}
           
            </div>
        @endforeach
    @endif
    @if(@Session::get('messages'))
        @foreach(Session::get('messages') as $message)
            <div class="alert alert-success" role="alert">
               
                {{$message}}
           
            </div>
        @endforeach
    @endif
@stop