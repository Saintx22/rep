@extends('user/layoutUsers')
@section('content')
    @if(@Session::get('messages'))
        @foreach(Session::get('messages') as $message)
            <div class="alert alert-success" role="alert">
               
                {{$message}}
           
            </div>
        @endforeach
    @endif
    @if(@Session::get('errors'))
        @foreach(Session::get('errors') as $message)
            <div class="alert alert-danger" role="alert">
               
                {{$message}}
           
            </div>
        @endforeach
    @endif
   <table class="table table-hover table-condensed table-bordered">
        <tr>
            <th>Cedula de Identidad</th>
            <th>Nombre de Usuario</th>
            <th>Email</th>
            <th>Permiso de gestión</th>
            <th>Fecha de Creaci&oacute;n</th>
            <th>Opciones</th>
        </tr>
        @foreach($Users as $user)
            <tr>
                <td>{{$user->ci}}</td>
                <td>{{$user->user}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->type}}</td>
                <td class="hidden-lg">{{date("d/m/Y", strtotime($user->created_at))}}</td>
                <td class="visible-lg">{{date("d/m/Y h:i:sa ", strtotime($user->created_at))}}</td>
                <td>
                    <p style="white-space: nowrap">
                        <button type="button" alt="RESTAURAR CONTRASEÑA" class="btn btn-warning carousel-btn-delete" data-toggle="modal" data-target=".restore{{$user->id}}">
                            <i class="glyphicon glyphicon-refresh"></i>
                        </button>                   
                        <button type="button" alt="ELIMINAR" class="btn btn-danger carousel-btn-delete" data-toggle="modal" data-target=".delete{{$user->id}}">
                            <i class="glyphicon glyphicon-trash"></i>
                        </button>                
                        <button type="button" alt="ROLS" class="btn btn-info carousel-btn-delete" data-toggle="modal" data-target=".rol{{$user->id}}">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </button>
                    </p>      
                </td>

                <!-- Un modal por cada usuario que locura :v pongamos 2-->
                <div class="modal fade delete{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Seguro que deseas eliminar a: {{$user->user}}</h4>
                      </div>
                      
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <a href="{{URL::to('admin/user/delete/'.$user->id)}}">
                            <button type="button" class="btn btn-danger">Eliminar</button>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade restore{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                  <div class="modal-dialog modal-sm">

                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Seguro que deseas restaurar la contrase&nacute;a a: {{$user->user}}</h4>
                      </div>
                      
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <a href="{{URL::to('admin/user/restore/'.$user->id)}}">
                            <button type="button" class="btn btn-warning">Restaurar</button>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade rol{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                      <div class="modal-header">
  {{Form::open(array('method' => 'POST', 'url' => 'admin/user/changeRol/'.$user->id, 'role' => 'form'))}}
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        
                        <h4 class="modal-title">Seguro que deseas modificar el permiso de: {{$user->user}}</h4>
                      </div>
                            <div class="col-xs-6 col-sm-7 col-md-6">

                                {{ Form::select('type', ['Administrador de Usuairos'=>'Administrador de Usuairos',
                                                         'Administrador de Publicacion'=>'Administrador de Publicación',
                                                         'Noticias'=>'Noticias',
                                                         'Pregrado'=>'Pregrado',
                                                         'Mutimedia'=>'Mutimedia'],
                                                          $user->type,['class' =>'form-control']) }}
                            </div>
                                    
                      <div class="modal-footer">
                        <button type='submit' class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-info">Modificar</button>
                        </a>

  {{Form::close()}}
                      </div>
                    </div>
                  </div>
                </div>

            </tr>

        @endforeach
    </table>

    {{$Users->links()}}
    

    

@stop