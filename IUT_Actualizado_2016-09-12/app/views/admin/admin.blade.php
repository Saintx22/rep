<!doctype html>
<html lang="en">

<head>
    {{HTML::style('css/structure.css')}}
    {{HTML::style('css/bootstrap.min.css')}}
    {{HTML::style('css/font-awesome.min.css')}}
    {{HTML::style('css/global.css')}}
    @yield('css')

    <title>IUT - Men&uacute; Administrador</title>

</head>

<body>
    <center>

            
    <div class="container-fluid">
            <div class= "carousel-logo2">
                {{HTML::image('img/logo.png','',array('class' => 'img-responsive'))}}
            </div>
        <!-- $_SESSION['type']->type;BARRA OFICIAL -->
        <div class="row hidden-xs">
            <div class="col-sm-6 col-md-5 text-left"> {{HTML::image("img/logo-gbv.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-sm-2 col-sm-offset-1 col-md-offset-3 text-right">{{HTML::image("img/img-2_o.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-sm-2 text-right">{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}</div>
        </div>
    <div class="header">
        <div class="row visible-xs">
            <div class="col-xs-8 text-left"> {{HTML::image("img/logo-gbv.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-xs-2 text-right">{{HTML::image("img/img-2_o.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-xs-2 text-right">{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}</div>
        </div>
    <br>
    <h1>Menú de Administrador</h1>
    <br>
            <div class="userId2">
                <div>
                        {{--*/$value2 = Session::get('username');$rest = substr($value2, 10, -3);/*--}}
                        <b>Nombre de usuario:</b> {{$rest}}
                </div>
                <div>
                        {{--*/$value = Session::get('key');$rest1 = substr($value, 10, -3);/*--}}
                        <b>Carga:</b> {{$rest1}}
                </div>
            </div>

    </div>
   </div>
    <div style="width: 600px;">

        <div class="list-group">
            <br>
            
            @include('admin.adminMenuLayout')
            <br>
            <a class="list-group-item list-group-item-success" href="{{URL::to('/')}}" target="_blank"><i class="fa fa-list-alt"></i>&nbsp; Ver Web</a>
            <a class="list-group-item" href="{{URL::to('admin/user/changePassword')}}"><i class="glyphicon glyphicon-pencil"></i>&nbsp; Cambiar Contraseña</a>
            <a class="list-group-item" href="{{URL::to('admin/user/changePorfile')}}"><i class="glyphicon glyphicon-pencil"></i>&nbsp; Modificar Perfil</a>
            <a class="list-group-item list-group-item-danger" href="{{URL::to('logout')}}"><i class="fa fa-lock fa-fw"></i>&nbsp; Cerrar Sesi&oacute;n</a>
            
         
        </div>
    </div>

    <button class="btn {{(!file_exists('txt/available')) ? 'btn-danger' : 'btn-success'}}" title="Encender/Apagar" data-toggle="modal" data-target="#available">
    	<i class="glyphicon glyphicon-off"></i> {{(!file_exists('txt/available')) ? 'Apagada' : 'Encendida'}}
    </button>
    
    <br><br>


    <!-- Modal para Habilitar o Deshabilitar la web -->
	<div class="modal fade" id="available" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<!-- Encabezado -->
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"> {{(!file_exists('txt/available')) ? 'Habilitar' : 'Deshabilitar'}} la web para todo publico. </h4>
				</div>

				<!-- Cuerpo del Modal -->
				<div class="modal-body">
					
					 ¿Est&aacute; seguro de querer {{(!file_exists('txt/available')) ? 'habilitar' : 'deshabilitar'}} la web para todo el p&uacute;blico?.

				    {{Form::open(array('url' => URL::to('admin/available'), 'method' => 'post','class' => 'form-horizontal'))}}
				</div>

				<!-- Botones -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					
					{{Form::submit((!file_exists('txt/available')) ? 'Habilitar' : 'Deshabilitar',array('class' => (!file_exists('txt/available'))?'btn btn-success':'btn btn-danger'))}}
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
    
  
</center>



{{HTML::script('js/jquery-1.11.1.min.js')}}
{{HTML::script('js/bootstrap.min.js')}}
{{HTML::script('js/relacionAspecto.js')}}
@yield('js')
</body>
</html>
