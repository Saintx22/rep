@extends('admin.layoutAdmin')

@section('title') Actualizar Encabezado @stop

@section('content')

	{{Form::open(array('files' => true))}}
	<center>

		<div class="tbcimg">
			{{HTML::image("img/img-1.png",'',array('class'=>'img-responsive'))}}
		</div>
		<div class="tbcinput">
			{{Form::file('img-1') }}
		</div>
		<br>
		
		<div class="tbcimg">
			{{HTML::image("img/img-2.png",'',array('class'=>'img-responsive'))}}
		</div>
		<div  class="tbcinput">
			{{Form::file('img-2') }}
		</div>
		<br>
		
		<div class="tbcimg">
			{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}
		</div>
		<div class="tbcinput">
			{{Form::file('img-3') }}
		</div>
	  <br><br>
	   <hr>
	   {{HTML::link('admin','Atr&aacute;s', array('class' => 'btn btn-default')) }} 
	  {{Form::submit('Actualizar Encabezado', array('class' => 'btn btn-primary')) }} <br> 
	</center>
	{{Form::close()}}
@stop