@extends('admin.layoutAdmin')

@section('title') Carrusel Edit @stop

@section('content')

    
    <div class="row container">
        <br>
        <div class="row">
            <h3>Anuncio (Imagen):</h3>
            {{Form::open(array('files' => true, 'method' => 'post', 'url' => 'admin/indexPhoto/Upload')) }}
            <div class="col-xs-6">

                {{Form::file('img',array('class'=>'form-control'))}}

            </div>
            <div class="col-xs-6">
                <input type="submit" class="btn btn-success " value="Subir"> 
            </div>
            {{Form::close()}}
        </div>
        <hr>
        <div class="row">
            
            <h3>Hipervínculo (URL):</h3>
            {{Form::open(array('method' => 'post', 'url' => 'admin/indexPhoto/SetURL'))}}
                <div class="col-xs-6">

                    {{Form::text('url', Input::old('url'), 
                        array('placeholder' => 'http://google.com', 'class' => 'form-control')
                    )}}
                    <small 
                    @if(Session::get('message')['type']=='success')
                        class="text-success"
                    @elseif(Session::get('message')['type']=='error')
                        class="text-danger"
                    @endif
                    >
                    {{Session::get('message')['content']}}</small>

                </div>
                <div class="col-xs-6">
                    <input type="submit" class="btn btn-success " value="Actualizar"> 
                </div>
            {{Form::close()}}
            <div class="col-xs-12">

                <br>
                Hipervínculo Actual: 
                {{(@$photoIndexURL<>'#')? "<a href='$photoIndexURL' target='_blank'>
                                            <strong>$photoIndexURL</strong>
                                        </a>
                                        <a href='#' data-toggle='modal' data-target='#modalUnsetURL'>
                                            <i class='glyphicon glyphicon-remove text-danger' ></i>
                                        </a>
                                        ":"Sin Hipervínculo"}}
                

            </div>
            
        </div>
    
        <hr> 
        <div class="row">
            <h3>Mostrar:</h3>
            {{Form::open(array('method' => 'post', 'url' => 'admin/indexPhoto/SetDisplay')) }}
                <div class="col-xs-6 ">

                    {{Form::radio('mostrar','1',
                            (file_exists('img/indexPhoto/display-1.png'))
                    )}} <span class="text-success">Mostrar cada vez que se entre a la pag. "Inicio"</span>
                    <br>
                    {{Form::radio('mostrar','0',
                            (!file_exists('img/indexPhoto/display-1.png'))
                    )}} <span class="text-danger">No Mostrar nunca.</span>

                </div>
                <div class="col-xs-6">
                    <input type="submit" class="btn btn-success " value="Actualizar"> 
                </div>
                {{Form::close()}}
        </div>
        <hr>
        <div class="row">
            <h3>Imágen:</h3>
            <div class="col-xs-6 text-center">
                @if(file_exists('img/indexPhoto/display-1.png'))
               {{(@$photoIndexURL<>'#')? "<a href='$photoIndexURL' target='_blank'>":""}}
                    <img src="{{URL::to('img/indexPhoto/display-1.png?'.rand())}}" alt="INFORMACION" class="img-responsive">
                {{(@$photoIndexURL<>'#')?'</a>':''}}
                @else
                <h4 class="bg-danger">La imágen no permite mostrarse o no existe.</h4>
                @endif
            </div>
        </div>
        <hr>
    </div> 
    
       <!-- Delete Modal -->
    <div class="modal fade" id="modalUnsetURL" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
        <div class="modal-header"></div>
          <div class="modal-body">
            <p>¿Remover Hipervínculo de la Imagen de Alerta?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <a href="{{URL::to('admin/indexPhoto/UnsetURL')}}">
                <button type="button" class="btn btn-danger">Remover</button>
            </a>
          </div>
        </div>
      </div>
    </div>
@stop