@extends('admin.layoutAdmin')

@section('title') Gestionar Relacionados @stop

@section('content')

    <div class="row">
         <div class="col-sm-11">
            <button class="btn btn-info" title="agregar" data-toggle="modal" data-target="#addRelated"> 
                <i class="glyphicon glyphicon-plus-sign"></i> Agregar Relacionado
            </button>  
            
            {{--*/ $messageBag = @Session::get('messageBag') /*--}}
           
            @if($messageBag)
                <br><br>
                @foreach($messageBag['messages'] as $message)

                    <div class="alert alert-{{$messageBag['type']}}" role="alert">
                    {{$message}} 
                   
                    </div>
                @endforeach
            @endif
        </div>
    </div>
    <br>
    <div class="row ">
        <div class="col-sm-11">
        @if(@$allRelated)
            <table class="table table-bordered " >
                @foreach ($allRelated as $i=>$related)
                    @if(@$related)
                        <tr>
                            <!-- IMAGEN -->
                            <td>
                                <div class="col-sm-6 col-md-5 col-lg-4">
                                    <a href="{{$related['url']}}" target="_blank">
                                            {{HTML::image($related['img'].'?'.rand(),'',array('class'=>'img-responsive'))}}
                                    </a>
                                </div>
                            </td>
                            <td>
                                <a href="{{$related['url']}}" target="_blank" style="white-space: nowrap;">
                                    {{$related['url']}} <i class="glyphicon glyphicon-new-window small"></i>
                                </a>
                            </td>
                            <!-- BOTONES -->
                            <td>
                                <button class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#deleteRelated{{$i}}"> Eliminar </button>
                                
                                <!-- MODAL DELETE -->
                                <div class="modal fade" id="deleteRelated{{$i}}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Eliminar</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>¿Realmente deseas eliminar éste Relacionado?</p>
                                                {{HTML::image($related['img'].'?'.rand(),'',array('class'=>'img-responsive'))}}
                                            </div>
                                            <div class="modal-footer">
                                                {{Form::open(array('url' => URL::to('admin/deleteRelated'), 'method' => 'post','files' => true ,'class' => 'form-horizontal'))}}
                                                {{Form::hidden('path', $related['path'])}}
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                {{Form::submit('Eliminar',array('class' => 'btn btn-danger'))}}
                                                {{Form::close()}}
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </td>
                        </tr>
                    @endif
                @endforeach
            </table>
        @endif
        </div> <!-- col -->
    </div> <!-- row -->
    
</div>

<!-- Modal para Agregar -->
<div class="modal fade" id="addRelated" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <!-- Encabezado -->
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"> Agregar Relacionado </h4>
            </div>

            <!-- Cuerpo del Modal -->
            <div class="modal-body">
            {{Form::open(array('url' => URL::to('admin/addRelated'), 'method' => 'post','files' => true ,'class' => 'form-horizontal'))}}
                <div class="form-group">
                    {{Form::label('img', 'Imágen:', array('class' =>'col-sm-4 control-label'))}}
                    <div class="col-sm-8 ">
                        {{Form::file('img', array('class'=>'form-control '))}}
                    </div>
                </div>

                <div class="form-group">
                    {{Form::label('url', 'Hipervínculo(Link):', array('class' =>'col-sm-4 control-label'))}}
                    <div class="col-sm-8">
                        {{Form::text('url', '',array('placeholder' => 'http://google.com', 'class' => 'form-control'))}}
                        <small>Recuerda colocar el protocolo: "http://", "https://", etc. </small>
                    </div>
                </div>
            </div>

            <!-- Botones -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                {{Form::submit('Enviar',array('class' => 'btn btn-info'))}}
                {{Form::close()}}
            </div>
        </div>
    </div>



    
@stop