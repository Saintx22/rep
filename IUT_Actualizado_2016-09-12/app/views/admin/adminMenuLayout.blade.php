

@if(@Session::get('key')=='[{"type":"Administrador de Usuairos"}]')
<a class="list-group-item list-group-item-info" href="{{URL::to('admin/users')}}"><i class="fa fa-user"></i>&nbsp; Gestionar Usuarios </a>
@endif
@if(@Session::get('key')=='[{"type":"Administrador de Publicacion"}]')
<a class="list-group-item list-group-item-info" href="{{URL::to('admin/indexPhoto')}}"><i class="fa fa fa-exclamation-circle"></i>&nbsp; Advertencia Inicio de Página</a>
<a class="list-group-item list-group-item-info" href="{{URL::to('admin/topBarChange')}}"><i class="fa fa-list fa-fw"></i>&nbsp; Actualizar Encabezado </a>
<a class="list-group-item list-group-item-info" href="{{URL::to('admin/html')}}"><i class="fa fa-list fa-fw"></i>&nbsp; Gestionar HTML</a>
<a class="list-group-item list-group-item-info" href="{{URL::to('admin/links')}}"><i class="fa fa-link"></i>&nbsp; Gestionar Enlaces</a>
<a class="list-group-item list-group-item-info" href="{{URL::to('admin/related')}}"><i class="fa fa-share-alt"></i>&nbsp; Gestionar Relacionados</a>
 @endif

@if(@Session::get('key')=='[{"type":"Noticias"}]')
<a class="list-group-item list-group-item-info" href="{{URL::to('adminNews')}}"><i class="fa fa-newspaper-o fa-fw"></i>&nbsp; Gestionar Noticias</a>
<a class="list-group-item list-group-item-info" href="{{URL::to('admin/carouselEdit')}}"><i class="fa fa-caret-square-o-right"></i>&nbsp; Gestionar el Carrusel de Im&aacute;genes</a>
@endif

@if(@Session::get('key')=='[{"type":"Mutimedia"}]')
<a class="list-group-item list-group-item-info" href="{{URL::to('admin/multimedia')}}"><i class="fa fa-file-video-o"></i>&nbsp; Gestionar Multimedia</a>
@endif
@if(@Session::get('key')=='[{"type":"Pregrado"}]')
<a class="list-group-item list-group-item-info" href="{{URL::to('adminPregrado')}}"><i class="fa fa-file-powerpoint-o"></i>&nbsp; Gestionar Pregrados</a>
@endif