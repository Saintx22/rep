<!doctype html>
<html lang="en">

<head>
    {{HTML::style('css/structure.css')}}
    {{HTML::style('css/bootstrap.min.css')}}
    {{HTML::style('css/font-awesome.min.css')}}
    {{HTML::style('css/global.css')}}
    @yield('css')

    <title>@yield('title')</title>

</head>

<body>
   
    <div class="container-fluid ">
        
        <div class="header">
            <div class= "carousel-logo1">
                {{HTML::image('img/logo.png','',array('class' => 'img-responsive'))}}
            </div>
            <div class="row" id="row">
                <a href="{{URL::to('admin')}}">
                    <div class="col-md-12 navbar-header text-muted text-center"><h1><b>Menú de Administrado</b></h1></div>
                </a>
            </div>
            <div class="userId">
                <div>
                        {{--*/$value2 = Session::get('username');$rest = substr($value2, 10, -3);/*--}}
                        <b>Nombre de usuario:</b> {{$rest}}
                </div>
                <div>
                        {{--*/$value = Session::get('key');$rest1 = substr($value, 10, -3);/*--}}
                        <b>Carga:</b> {{$rest1}}
                </div>
            </div>
            <div class="bottomshead"> 
                <p><a class="text-muted " href="{{URL::to('logout')}}">
                    <i class="fa fa-lock fa-fw" style="font-size:19px;color:#000033" data-toggle="tooltip" data-placement="top" title="Cerrar Sesión"></i>
                </a><p>
               <a class="text-muted" href="{{URL::to('admin/user/changePorfile')}}">
                    <i class="glyphicon glyphicon-pencil" style="font-size:19px;color:#00FFFF" data-toggle="tooltip" data-placement="top" title="Modificar"></i>
                    </a>
            </div>
        </div>
        <hr>
        
        <!--MENU ADMINISTRADOR-->
        <div class="row2">
            <div class="col-sm-3" id="barra">
                <div class="list-group text-center">
                    <br>
                    @include('admin.adminMenuLayout')     
                    <hr>
                    @yield('options')    
                    <a class="list-group-item" href="{{URL::to('admin')}}"><i class="fa fa-chevron-left"></i>&nbsp; Atrás</a>   
                    <a class="list-group-item" href="{{URL::to('/')}}" target="_blank"><i class="fa fa-list-alt"></i>&nbsp; Ver Web</a>
                </div>
            </div>
        
            <!--CONTENIDO-->
            <div class="col-sm-9">
                @yield('content')
            </div>
        </div>
    </div>
  

{{HTML::script('js/jquery-1.11.1.min.js')}}
{{HTML::script('js/bootstrap.min.js')}}
{{HTML::script('js/relacionAspecto.js')}}
@yield('js')
</body>
</html>
