<!doctype html>
<html lang="en">

<head>
    {{HTML::style('css/bootstrap.min.css')}}
    {{HTML::style('css/font-awesome.min.css')}}
    {{HTML::style('css/global.css')}}
    @yield('css')

    <title>IUT - Men&uacute; Administrador</title>

</head>

<body>
    <center>

    <div class="container-fluid">
        <!-- BARRA OFICIAL -->
        <div class="row hidden-xs">
            <div class="col-sm-6 col-md-5 text-left"> {{HTML::image("img/img-1.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-sm-2 col-sm-offset-1 col-md-offset-3 text-right">{{HTML::image("img/img-2.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-sm-2 text-right">{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}</div>
        </div>
        <div class="row visible-xs">
            <div class="col-xs-8 text-left"> {{HTML::image("img/img-1.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-xs-2 text-right">{{HTML::image("img/img-2.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-xs-2 text-right">{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}</div>
        </div>
   </div>
    <br>
    <br>
    <div class="container">
    
        
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Ingresar</h3></div>
                <div class="panel-body">
    
                        
                    {{ Form::open(array('class'=>'form-horizontal')) }}

                        <div class="form-group @if(@Session::get('errors')) 
                                        {{(@Session::get('errors')->get('user'))? 'has-error':'';}} 
                                    @endif">
                            {{ Form::label('user', 'Usuario', array('class' =>'col-xs-3 control-label')) }}
                            <div class="col-xs-6 col-sm-7 col-md-6">
                                {{ Form::text('user', Input::old('user'), array('class' =>'form-control')) }}
                            </div>
                        </div>

                         <div class="form-group  @if(@Session::get('errors')) 
                                        {{(@Session::get('errors')->get('password'))? 'has-error':'';}} 
                                    @endif">     
                            {{ Form::label('password', 'Contrase&ntilde;a', array('class' =>'col-xs-3 control-label')) }}
                            <div class="col-xs-6 col-sm-7 col-md-6">
                                <input type="password" name="password" id="password" value="{{Input::old('password')}}" class="form-control">
                            </div>
                        </div>

                            <a href="{{URL::to('/')}}" class="btn btn-default">Salir</a>
                            {{ Form::submit('Acceder', array('class' => 'btn btn-primary')) }}
                    {{ Form::close() }}
                    <br>
                    <!-- if there are login errors, show them here -->
                    @if (@Session::get('errors'))
                        @foreach(Session::get('errors')->all() as $message)
                            <div class="alert alert-danger" role="alert">
                           
                                {{$message}}
                           
                            </div>
                        @endforeach
                           
                    @endif
                </div>
            </div>
        </div>
    </div>
    
</center>

{{HTML::script('js/jquery-1.11.1.min.js')}}
{{HTML::script('js/bootstrap.min.js')}}
{{HTML::script('js/relacionAspecto.js')}}
@yield('js')
</body>
</html>
