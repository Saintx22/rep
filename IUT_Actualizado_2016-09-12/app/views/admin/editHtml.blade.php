@extends('admin.layoutAdmin')

@section('title') Gestionar pantallas HTML @stop

@section('content')
	
	{{Form::open(array('url' => URL::to('admin/html/'.$name), 'method' => 'post'))}}

 	 	<div class="form-group">	
 	 		<a href="{{URL::to('admin/html/'.$name)}}" class="btn btn-success"> Deshacer</a>

			{{Form::submit('Actualizar',array('class'=>'btn btn-default'))}}
			
		</div>

	 	<div class="form-group">
		  <textarea class="ckeditor" rows="30" name="content">{{File::get('../app/views/html/'.$name)}}</textarea>
		</div>
			<a href="{{URL::to('admin')}}"><button type="button" class="btn btn-warning" data-dismiss="modal" >Cancelar</button></a>


	{{Form::close()}}


@stop
@section('js')
<script src="{{ asset('../public_html/js/ckeditor/ckeditor.js') }}"></script>
<!--<script src="../../../../public_html/js/tinymce/js/tinymce/tinymce.min.js"</script>-->
<script src="../../../../public_html/js/bootstap.min.js"</script>
<!--<script type="text/javascript">
    tinyMCE.init({
    selector: "textarea"
    });
</script>-->
<script src="../../public_html/js/jquery-1.11.1.min.js"</script>
@stop
@stop