@extends('pregrado/layoutPregrado')
@section('content')
<h1> Modificar Pregrado </h1>
 <!-- if there are login errors, show them here -->
<span class="inpError">{{$errors->first()}}</span>

   
    {{ Form::open(array('class'=>'form-horizontal','files' => true)) }}

        <div class="form-group" data-toggle="tooltip" data-placement="top" title="Min 5; Max 256 caracteres; Requerido">
            {{ Form::label('name', 'Nombre de la carrera: ', array('class' =>' control-label')) }}
                {{ Form::text('name', $pregrado->name, array('class' =>'form-control')) }}
        </div>
    
        <div class="form-group" data-toggle="tooltip" data-placement="top" title="Min 200 caracteres">
            {{ Form::label('content', 'Contenido: ', array('class' =>' control-label')) }}
                {{ Form::textarea('contentpath',File::get($pregrado->contentPath), array('class' =>'ckeditor')) }}
        </div>
        {{ Form::submit('Modificar', array('class' => 'btn btn-primary col-xs-offset-3')) }}
        <a href="{{URL::to('searchPregrado')}}"><button type="button" class="btn btn-warning" data-dismiss="modal" >Cancelar</button></a>

    {{ Form::close() }}

@stop
@stop
@section('js')

<script src="{{ asset('../public_html/js/ckeditor/ckeditor.js') }}"></script>
<!--<script src="../../../public_html/js/tinymce/js/tinymce/tinymce.min.js"</script> -->
<script src="../../../public_html/js/jquery-1.11.1.min.js"</script>
<!--<script type="text/javascript">
    tinyMCE.init({
    selector: "textarea"
    });
</script> -->
<script src="../../../public_html/js/bootstap.min.js"</script>
@stop