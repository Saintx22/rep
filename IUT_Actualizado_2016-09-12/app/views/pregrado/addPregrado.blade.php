@extends('pregrado/layoutPregrado')
@section('content')
<h1> A&ntilde;adir Pregrado </h1>
 <!-- if there are login errors, show them here -->
        @if(@Session::get('messages'))
            @foreach(Session::get('messages') as $message)
                <div class="alert alert-success" role="alert">
                   
                    {{$message}}
               
                </div>
            @endforeach
        @endif
        @if(@Session::get('errors'))
            @foreach(Session::get('errors')->all() as $message)
                <div class="alert alert-danger" role="alert">
                   
                    {{$message}}
               
                </div>
            @endforeach
        @endif
    {{ Form::open(array('files' => true)) }}

        <div class="form-group" data-toggle="tooltip" data-placement="top" title="Min 5; Max 256 caracteres; Requerido">
            {{ Form::label('name', 'Nombre del PNF: ', array('class' =>' control-label')) }}
                {{ Form::text('name', Input::old('name'), array('class' =>'form-control')) }}
        </div>
    
        <div class="form-group" data-toggle="tooltip" data-placement="top" title="Min 200 caracteres; Requerido">
            {{ Form::label('content', 'Contenido: ', array('class' =>' control-label')) }}
                {{ Form::textarea('content', Input::old('content'), array('class' =>'ckeditor')) }}
        </div>
            
        {{ Form::submit('A&ntilde;adir', array('class' => 'btn btn-primary ')) }}
        <a href="{{URL::to('adminPregrado')}}"><button type="button" class="btn btn-warning" data-dismiss="modal" >Cancelar</button></a>
       
    {{ Form::close() }}
@stop

@section('js')
<script src="{{ asset('../public_html/js/ckeditor/ckeditor.js') }}"></script>
<!--<script type="text/javascript" src="../../public_html/js/tinymce/js/tinymce/tinymce.min.js"</script>-->
<script type="text/javascript" src="../../public_html/js/bootstap.min.js"</script>
<!--<script type="text/javascript">
    tinyMCE.init({
          selector: "textarea"
    });
</script>-->
<script type="text/javascript" src="../../public_html/js/jquery-1.11.1.min.js"</script>
@stop