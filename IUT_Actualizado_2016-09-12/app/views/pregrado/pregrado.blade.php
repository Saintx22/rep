@extends('layout')
@section('content')

<div class="col-sm-12 text-justify">
   <h1 class="media-heading"> {{$pregrado->name}}</h1> 
    <hr>
    <p>{{str_replace("\n","<br>",File::get($pregrado->contentPath))}}</p>
    <a href="{{URL::to('download/pdf/'.$pregrado->id)}}" target="_blank">Descargar en PDF</a>
    <a href="{{URL::to('/')}}"><button class="btn btn-default pull-right"> Volver al Inicio</button></a>
    
</div>

@stop       
