@extends('admin.layoutAdmin')

@section('title') Gestionar Pregrados @stop

@section('options')
                    <a class="list-group-item list-group-item-success" href="{{URL::to('addPregrado')}}"><i class="fa fa-file-image-o fa-fw"></i>&nbsp; A&ntilde;adir Pregrado</a>
                    <a class="list-group-item list-group-item-info" href="{{URL::to('searchPregrado')}}"><i class="fa fa-newspaper-o fa-fw"></i>&nbsp; Ver todos los Pregrado </a>
                    <hr>
@stop
