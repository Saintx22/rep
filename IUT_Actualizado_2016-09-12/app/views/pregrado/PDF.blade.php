
<div class="col-sm-12 text-justify">
	<h1 class="media-heading"> {{$pregrado->name}}</h1> 
    <div align="right">Caracas {{date("F j, Y, g:i a")}}</div>
	<hr>
    <p>{{str_replace("\n","<br>",File::get($pregrado->contentPath))}}</p>
    
</div>
