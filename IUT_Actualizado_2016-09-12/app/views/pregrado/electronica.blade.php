@extends('layout')
@section('content')

<h1> PNF en Electr&oacute;nica </h1>
<hr>
<h3> Misión </h3>
   <p> Formar profesionales  con carácter social, íntegros y altamente calificados para desempeñarse  de manera  competente en las distintas  áreas  tecnológicas como:Electrónica, Instrumentación y control, Electricidad Industrial y Potencia  y telecomunicaciones.</p>

<h3> Visión </h3>
    <p>Ser un ente  de prestigio capaz de responder en  forma eficiente a las exigencias académicas y administrativas, fundamentado en los  principios modernos de la Educación Superior, que genere políticas de docencia, investigación aplicada, asistencia  técnica, curso de especialización, extensión y postgrado.</p> 

<h3> Estructura </h3> 

<div class="col-md-9">
    
    <table class="table table-bordered">
        <tr>
            <td colspan="4" class="active">
                <a href="{{URL::to('pregrado/electronica/info/ingeniero')}}">Ingeniero en Electr&oacute;nica</a>
            </td>
        </tr>
        <tr>
            <td>Trayecto 2</td>
            <td>Trimestre 1</td>
            <td>Trimestre 2</td>
            <td>Trimestre 3</td>
        </tr>
        <tr>
            <td>Trayecto 1</td>
            <td>Trimestre 1</td>
            <td>Trimestre 2</td>
            <td>Trimestre 3</td>
        </tr>
        <tr>
            <td>Trayecto Transición T.S.U.</td>
            <td colspan="3"> Trimestre (12 Semanas)</td>
        </tr>
        <tr>
            <td colspan="4" class="active">
                <a href="{{URL::to('pregrado/electronica/info/tsu')}}">T.S.U. en Electr&oacute;nica</a>
            </td>
        </tr>
        <tr>
            <td>Trayecto 2</td>
            <td>Trimestre 1</td>
            <td>Trimestre 2</td>
            <td>Trimestre 3</td>
        </tr>
        <tr>
            <td colspan="4" class="active">
                <a href="{{URL::to('pregrado/electronica/info/asistente')}}">Asistente en Electrónica</a>
            </td>
        </tr>
        <tr>
            <td>Trayecto 1</td>
            <td>Trimestre 1</td>
            <td>Trimestre 2</td>
            <td>Trimestre 3</td>
        </tr>
        <tr>
            <td>Trayecto Inicial</td>
            <td colspan="3">Trimestre (12 Semanas)</td>
        </tr>
    </table>
</div>
@stop

