@extends('pregrado/layoutPregrado')
@section('content')
        @if(@Session::get('messages'))
            @foreach(Session::get('messages') as $message)
                <div class="alert alert-success" role="alert">
                   
                    {{$message}}
               
                </div>
            @endforeach
        @endif
	   @if(@Session::get('errors'))
            @foreach(Session::get('errors')->all() as $message)
                <div class="alert alert-danger" role="alert">
                   
                    {{$message}}
               
                </div>
            @endforeach
        @endif

        <h2> Pregrados </h2>
        @foreach($pregradoArray as $pregrado)
        <div class="media media-new col-md-6">
            <div class="media-body text-justify">
                <h3 class="media-heading"> {{$pregrado->name}}</h3>
                
                <a href="{{URL::to('deletePregrado/'.$pregrado->id)}}"><span class="btn btn-danger carousel-btn-delete">Eliminar Pregrado</span></a>
                <a href="{{URL::to('pregrado/'.$pregrado->id)}}" target="_blank"><span class="btn btn-info btn carousel-btn-delete">Ver</span></a>
                <a href="{{URL::to('changePregrado/'.$pregrado->id)}}"><span class="btn btn-warning carousel-btn-delete">Modificar Pregrado</span></a>
            </div>
        </div> 
        @endforeach
        

@stop       
