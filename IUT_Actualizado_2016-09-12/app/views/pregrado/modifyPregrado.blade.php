@extends('pregrado/layoutPregrado')
@section('content')
<h1> Modificar Pregrado </h1>
 <!-- if there are login errors, show them here -->
<span class="inpError">{{$errors->first()}}</span>

   
    {{ Form::open(array('class'=>'form-horizontal','files' => true)) }}

        <div class="form-group">
            {{ Form::label('name', 'Nombre de la carrera: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6">
                {{ Form::text('name', $pregrado->name, array('class' =>'form-control')) }}
            </div>
        </div>
    
        <div class="form-group">
            {{ Form::label('content', 'Contenido: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6">
                {{ Form::textarea('content', File::get($pregrado->contentPath), array('class' =>'form-control')) }}
            </div>
        </div>
            
        {{ Form::submit('Modificar', array('class' => 'btn btn-primary col-xs-offset-3')) }}
       
    {{ Form::close() }}

             
   

@stop