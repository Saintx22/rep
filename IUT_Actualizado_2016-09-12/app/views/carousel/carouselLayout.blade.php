@extends('admin.layoutAdmin')

@section('title') Gestionar Carrusel @stop

@section('options')

    <a class="list-group-item list-group-item-success" href="{{URL::to('admin/newPicture')}}"><i class="fa fa-plus-square-o"></i>&nbsp; A&ntilde;adir Imagen</a>
    <a class="list-group-item list-group-item-info" href="{{URL::to('admin/carouselEdit')}}"><i class="fa fa-file-image-o"></i>&nbsp; Ver todas las imagenes del Carrusel</a>
    <br>
    <div class="row">
        <div class="col-sm-12">
            {{Form::open(array('url'=>'admin/switchCarousel','id' => 'switchCarousel', 'files' => false))}}
                <h4 class="text-info">Funcionamiento del Carrusel:</h4>
                <div class="btn-group" data-toggle="buttons">
                    <label onclick="sendSwitchCarousel()" class="btn btn-info @if(file_exists('txt/carouselActivated')) active @endif">
                        <input type="radio" name="carouselSwitch" id="option1" autocomplete="off" value="1"  checked> Activado
                    </label>
                    <label onclick="sendSwitchCarousel()" class="btn btn-info @if(!file_exists('txt/carouselActivated')) active @endif">
                        <input type="radio" name="carouselSwitch" id="option3" autocomplete="off" value="0" > Desactivado
                    </label>
          
                </div>
            {{Form::close()}}
        </div>
    </div>
    @if(file_exists('txt/carouselActivated'))
        <h4 class="bg-success text-success">Carrusel activado y en uso.</h4>
        <small class="text-info"><i class="glyphicon glyphicon-warning-sign"></i> El Carrusel se está mostrando en la página web.</small>
    @else
        <h4 class="bg-danger text-danger">Carrusel desactivado.</h4>
        <small class="text-info"><i class="glyphicon glyphicon-warning-sign"></i> Sí el carrusel está desactivado, se utilizará el carrusel del Ministerio del Poder Popular para Educación Universitaria.</small>
    @endif

    <hr>
@stop

