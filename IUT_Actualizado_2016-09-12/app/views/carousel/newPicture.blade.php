@extends('carousel.carouselLayout')

@section('title') Carousel Edit @stop

@section('content')

	<div class="">

         @if(@Session::get('errors'))
            @foreach(Session::get('errors')->all() as $message)
                <div class="alert alert-danger" role="alert">
                   
                    {{$message}}
               
                </div>
            @endforeach
        @endif
		<div class="row container">

			<br>
			<div class="row container form-horizontal">
				{{Form::open(array('files' => true, 'method' => 'post')) }}
				
				<div class="form-group @if(@Session::get('errors')) 
		                                   {{(@Session::get('errors')->get('position'))? 'has-error':'';}} 
		                               @endif">
		            {{ Form::label('position', 'Nombre:', array('class' =>'col-sm-2 control-label')) }}
		            <div class="col-xs-6">
		                {{ Form::text('position', Input::old('position'), array('class' =>'form-control')) }}
		                <small>Las Imágenes apareceran según orden alfabético o numérico.</small>
		            </div>
		            <div class=".form-inline">
		                {{ Form::label('req', '*')}}
		            </div>
		        </div>


		        <div class="form-group @if(@Session::get('errors')) 
		                                   {{(@Session::get('errors')->get('link'))? 'has-error':'';}} 
		                               @endif">
		            {{ Form::label('link', 'Hiperv&iacute;nculo(Link):', array('class' =>'col-sm-2 control-label')) }}
		            <div class="col-xs-6">
		                {{ Form::text('link', Input::old('link'), array('class' =>'form-control')) }}
		            </div>
		        </div>
				
				<div class="form-group @if(@Session::get('errors')) 
		                                   {{(@Session::get('errors')->get('img'))? 'has-error':'';}} 
		                               @endif">
					{{ Form::label('img', 'Selecciona la Imagen', array('class' =>'col-sm-2 control-label')) }}
					<div class="col-xs-6">
					{{Form::file('img',array('class'=>'form-control'))}}
					</div>
		            <div class=".form-inline">
		                {{ Form::label('req', '*')}}
		            </div>	
				</div>

				<div class="form-group @if(@Session::get('errors')) 
		                                   {{(@Session::get('errors')->get('link'))? 'has-error':'';}} 
		                               @endif">
		            {{ Form::label('date', 'Tiempo limitado:', array('class' =>'col-sm-2 control-label')) }}
		            <div class="col-xs-6">
		            {{ Form::label('date', 'Fecha:', array('class' =>'col-sm-2 control-label')) }}
		                {{Form::input('date', 'date', null, ['class' =>'form-control','min' => date('Y-m-d')]);}}
		            </div>
		        </div>
				
				


				<input type="submit" class="btn btn-success col-sm-offset-4" value="Agregar"> 
			
				{{Form::close()}}

			</div>
			<hr>
		</div>
	</div>
@stop