@extends('carousel.carouselLayout')

@section('content')
             
            
    {{--*/ $i=1/*--}}
    {{--*/ $elementosPorLinea=3/*--}}
    @foreach ($array as $key=>$x)

            {{($i==1)? '<div class="row">':'';}}
            <div class="col-xs-4" >

                {{HTML::image($x->imgPath,'',array('class'=>'img-responsive'))}}

                <button type="button" class="btn btn-danger btn-xs col-xs-12 carousel-btn-delete" data-toggle="modal" data-target=".delete{{$x->position}}">Eliminar</button>

            </div>
            <!-- Modal -->
             <div class="modal fade delete{{$x->position}}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Seguro que deseas eliminar esta imagen? </h4>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <a href="{{URL::to('admin/eliminar/'.$x->position)}}">
                                    <button type="button" class="btn btn-danger">Eliminar</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
       {{($i==$elementosPorLinea)? '</div>':'';}}
       {{--*/ $i=($i==$elementosPorLinea)?1:$i+1/*--}}

    @endforeach
            
    {{($i>1 && $i<=$elementosPorLinea)? '</div>':'';}}
     
@stop

@section('js')
        <script>
        function sendSwitchCarousel(){
                setTimeout(function(){
                        $('#switchCarousel').submit();
                },100);
        }
        </script>
@stop