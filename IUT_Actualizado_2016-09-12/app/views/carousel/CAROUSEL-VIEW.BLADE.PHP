<!-- CAROUSEL -->
            
           {{--*/ $path= 'img/carousel/' /*--}}
           {{--*/ $dir= scandir($path) /*--}}
           {{--*/ $supported_images = array('gif','jpg','jpeg','png','bmp');/*--}}
           {{--*/ $i=0/*--}}
           <div class="row">

            <div class="container-fluid" >
                <div class="">
                    <div id="carouselIUT" class="carousel slide" data-ride="carousel" >
                        <!-- Indicadores -->

                        <ol class="carousel-indicators">
                            @foreach ($dir as $file)
                                @if(in_array(strtolower(pathinfo($file)['extension']),$supported_images))
                                    <li data-target="#carouselIUT" data-slide-to="0" class="{{($i++==0)?'active':''}}"></li>
                                @endif
                            @endforeach
                            {{--*/ $i=0/*--}}
                        </ol>
                        
                        <!-- Contenedores de slide -->
                        <div class="carousel-inner " role="listbox" >

                            <div class= "carousel-logo col-xs-4 col-sm-3 col-sm-2">
                               {{HTMl::image('img/logo.png','',array('class' => 'img-responsive'))}}
                            </div>
                            <div class= "carousel-title col-sm-10 col-md-7 col-xs-offset-4 col-sm-offset-3 col-sm-offset-2 hidden-xs">
                               <h3>INSTITUTO UNIVERSITARIO DE TECNOLOGIA <br> "Dr Federico Rivero Palacio"</h3>
                            </div>
                            @foreach ($dir as $file)
                                @if(in_array(strtolower(pathinfo($file)['extension']),$supported_images))
                                   <div class="item {{($i++==0)?'active':''}}">
                                        {{HTML::image($path.$file)}}
                                        <div class="carousel-caption">
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            <!-- Controles -->
                            <a href="#carouselIUT" class="left carousel-control" role="button" data-slide="prev">
                               <i class="glyphicon glyphicon-chevron-left"></i>
                            </a>
                            <a href="#carouselIUT" class="right carousel-control" role="button" data-slide="next">
                                <i class="glyphicon glyphicon-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <!--END CAROUSEL -->