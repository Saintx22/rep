@extends('layout')
@section('content')
<div class="col-sm-12 text-justify">
    <h2 class="media-heading"> {{$news->title}} </h2> 
    <br>
    
    @if(@file_exists($news->imgPath))
        <div class="small muted img-new" data-toggle="modal" data-target="#modalNewsPhoto">
            <a >
                {{HTML::image($news->imgPath,'',array('class'=>'img-responsive '))}}
            </a>
              
            <em>{{$news->imgFoot}}</em>
        </div>
    @endif
    <p>{{str_replace("\n","<br>",$news->content)}}</p>

    <p class="text-muted">{{$news->author}} {{($news->created_at<>'0000-00-00')?"| ".$news->created_at->format('Y-m-d'):''}}</p>
    <a onClick="window.history.back();"><button class="btn btn-default pull-right"> Volver</button></a>

</div>

@if(file_exists($news->imgPath))
<!-- Modal Añadir Usuario -->
    <div class="modal fade" id="modalNewsPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
      <div class="modal-dialog" role="document" style="width: initial; max-width:80%">
        <div class="modal-content">
          <div class="modal-body">
            <center>
               <img src="{{URL::to($news->imgPath)}}" 
                    alt="{{$news->imgFoot}}" 
                    class="img-responsive"
                    style="margin:0;"
                    >
            </center>
          </div>
          <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            {{Form::close()}}
          </div>
        </div>
      </div>
    </div>
@endif
  @if($news->version>1)
    <h4 class="media-heading">Versiones anteriores a esta misma noticia:</h4> 
        @for($i =($news->version-1); $i > 0; $i--)
            <p><a href="{{URL::to('news/version/'.$news->codename.'/'.$i)}}" class=""><ins>Versión {{$i}}: {{$news->title}}</ins></button></a></p>
        @endfor
       {{--*/ $newsArr = News::where('codename',$news->codename);/*--}}
  @endif
@stop       
