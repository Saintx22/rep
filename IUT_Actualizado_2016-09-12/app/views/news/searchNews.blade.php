@extends('news/layoutNews')
@section('content')
        @if(@Session::get('messages'))
            @foreach(Session::get('messages') as $message)
                <div class="alert alert-success" role="alert">
                   
                    {{$message}}
               
                </div>
            @endforeach
        @endif
        @if(@Session::get('errors'))
            @foreach(Session::get('errors')->all() as $message)
                <div class="alert alert-danger" role="alert">
                   
                    {{$message}}
               
                </div>
            @endforeach
        @endif
        <h2> Noticias </h2>
        {{--*/ $i=1/*--}}
        {{--*/ $elementosPorLinea=2/*--}}
        @foreach($newsArray as $news)
            {{($i==1)? '<div class="row" style="margin-top: 15px">':'';}}
            <div class="media media-new col-md-6 {{(Session::get('newsAdded')==$news->id)?'bg-success':''}}" style="margin-top: 0px">
                @if(@file_exists($news->imgPath))
                    <div class="media-left">
                        <a href="{{$news->imgPath}}" target="_blank">
                            {{HTML::image($news->imgPath,'',array('class'=>'media-object'))}}
                        </a>
                        
                    </div>
                @endif
                <div class="media-body text-justify">
                    <h4 class="media-heading"> {{$news->title}}</h4>
                    <p>{{substr($news->content,0,127)}}...</p>
                </div>
                <p class="text-muted">{{$news->author}} {{($news->created_at<>'0000-00-00')?"| ".$news->created_at->format('Y-m-d'):''}}</p>
                    <a href="{{URL::to('news/'.$news->id)}}" target="_blank"><span class="btn btn-info carousel-btn-delete">Ver</span></a>
                    <a href="{{URL::to('changeNews/'.$news->id)}}"><span class="btn btn-warning carousel-btn-delete">Modificar Noticia</span></a>
                    <a data-toggle="modal" data-target="#deleteNews{{$news->id}}"><span class="btn btn-danger carousel-btn-delete">Eliminar Noticia</span></a>
            </div> 
            
            <!-- MODAL ELIMINAR  -->
            <div class="modal fade" id="deleteNews{{$news->id}}" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Eliminar</h4>
                        </div>
                        <div class="modal-body">
                            <p>¿Realmente deseas <strong>eliminar</strong> la noticia "<em>{{$news->title}}</em>"?</p>
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <a href="{{URL::to('deleteNews/'.$news->id)}}"><button class="btn btn-danger">Eliminar Noticia</button></a>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        {{($i==$elementosPorLinea)? '</div>':'';}}
        {{--*/ $i=($i==$elementosPorLinea)?1:$i+1/*--}}
    @endforeach

    {{($i>1 && $i<=$elementosPorLinea)? '</div>':'';}}
        
    {{$newsArray->links()}}

@stop       
