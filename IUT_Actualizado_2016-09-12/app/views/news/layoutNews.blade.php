@extends('admin.layoutAdmin')

@section('title') Gestionar Noticias @stop

@section('options')
    <a class="list-group-item list-group-item-success" href="{{URL::to('addNews')}}"><i class="fa fa-file-image-o fa-fw"></i>&nbsp; A&ntilde;adir Noticia</a>
    <a class="list-group-item " href="{{URL::to('searchNews')}}"><i class="fa fa-newspaper-o fa-fw"></i>&nbsp; Ver todas las Noticias </a>
      <hr> 
@stop

