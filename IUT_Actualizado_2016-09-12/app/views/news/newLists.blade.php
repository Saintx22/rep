{{--*/ $i=1/*--}}
{{--*/ $elementosPorLinea=2/*--}}
@foreach($newsArray as $news)
    {{($i==1)? '<div style="display:table; margin-top: 15px">':'';}}
    <div class="col-xs-12 col-md-6">
        <div class="media media-new" style="margin-top: 0px">
            @if(@file_exists($news->imgPath))
                <div class="media-left">
                    <a href="{{$news->imgPath}}">
                        {{HTML::image($news->imgPath,'',array('class'=>'media-object'))}}
                    </a>
                </div>
            @endif
            <div class="media-body text-justify">
                <h4 class="media-heading"> {{$news->title}}</h4>
                <p>{{substr($news->content,0,127)}}... <a href="{{URL::to('news/'.$news->id)}}" class=""><ins>Seguir leyendo</ins></button></a></p>
                
            </div>
        <p class="text-muted"> Pie de Imagen: {{$news->imgFoot}}</p>
        <p class="text-muted">{{$news->author}} {{($news->created_at<>'0000-00-00')?"| ".$news->created_at->format('Y-m-d'):''}}</p>
        
        </div> 
    </div>
    {{($i==$elementosPorLinea)? '</div>':'';}}
    {{--*/ $i=($i==$elementosPorLinea)?1:$i+1/*--}}
@endforeach

{{($i>1 && $i<=$elementosPorLinea)? '</div>':'';}}

