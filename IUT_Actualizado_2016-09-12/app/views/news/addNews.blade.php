@extends('news/layoutNews')
@section('content')
<h1> A&ntilde;adir Noticia </h1>
  
    {{ Form::open(array('class'=>'form-horizontal','files' => true)) }}

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('title'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('title', 'T&iacute;tulo: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6" data-toggle="tooltip" data-placement="top" title="Min 5; Max 256 caracteres">
                {{ Form::text('title', Input::old('title'), array('class' =>'form-control', 'maxlength'=> '256')) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>  
        </div>

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('content'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('content', 'Contenido: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-12 col-sm-12 col-md-9" data-toggle="tooltip" data-placement="top" title="Min 200 caracteres">
                {{ Form::textarea('content', Input::old('content'), array('class' =>'ckeditor', 'spellchecker'=>'true')) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>  
        </div>

        <div class="form-group">
            {{ Form::label('img', 'Imagen: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6" data-toggle="tooltip" data-placement="top" title="Solo extensiones tipo .PNG o .JPEG">
                {{Form::file('img')}}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('imgFoot', 'Pie de la imagen: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6" data-toggle="tooltip" data-placement="top" title="Min 3 caracteres">
                {{ Form::text('imgFoot', Input::old('imgFoot'), array('class' =>'form-control')) }}
            </div>
        </div>

        <div class="form-group @if(@Session::get('errors')) 
                                   {{(@Session::get('errors')->get('author'))? 'has-error':'';}} 
                               @endif">
            {{ Form::label('author', 'Autor: ', array('class' =>'col-xs-2 control-label')) }}
            <div class="col-xs-6 col-sm-7 col-md-6" data-toggle="tooltip" data-placement="top" title="Min 5; Max 128 caracteres">
                {{ Form::text('author', Input::old('author'), array('class' =>'form-control', 'maxlength'=> '128')) }}
            </div>
            <div class=".form-inline">
                {{ Form::label('req', '*')}}
            </div>  
        </div>
            
            {{ Form::submit('A&ntilde;adir', array('class' => 'btn btn-primary col-xs-offset-3')) }}
            <a href="{{URL::to('adminNews')}}"><button type="button" class="btn btn-warning" data-dismiss="modal" >Cancelar</button></a>

    {{ Form::close() }}
    <br>
<!-- if there are errors, show them here -->
 @if(@Session::get('errors'))
    @foreach(Session::get('errors')->all() as $message)
        <div class="alert alert-danger" role="alert">
           
            {{$message}}
       
        </div>
    @endforeach
@endif
             
@stop

@section('js')
<script src="{{ asset('../public_html/js/ckeditor/ckeditor.js') }}"></script>
<!--<script type="text/javascript" src="../../public_html/js/tinymce/js/tinymce/tinymce.min.js"</script>-->
<script type="text/javascript" src="../../public_html/js/bootstap.min.js"</script>
<!--<script type="text/javascript">
    tinyMCE.init({
          selector: "textarea"
    });
</script>-->
<script type="text/javascript" src="../../public_html/js/jquery-1.11.1.min.js"</script>
@stop