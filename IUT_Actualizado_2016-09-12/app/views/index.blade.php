@extends('layout')

    @section('content')

<!-- CAROUSEL -->
     @if(file_exists('txt/carouselActivated'))
       {{--*/ $supported_images = array('gif','jpg','jpeg','png','bmp');/*--}}
       {{--*/ $i=0/*--}}
        <div class="container-fluid" >
            <div class="row">
                <div id="carouselIUT" class="carousel slide" data-ride="carousel" >
                    <!-- Indicadores -->

                    <ol class="carousel-indicators">
                        @foreach ($dir as $file)
                            @if(in_array(strtolower($file->getExt()),$supported_images))
                                <li data-target="#carouselIUT" class="{{($i==0)?'active':''}}" data-slide-to="{{$i++}}"></li>
                            @endif
                        @endforeach
                        {{--*/ $i=0/*--}}
                    </ol>
                    
                    <!-- Contenedores de slide -->
                    <div class="carousel-inner " role="listbox" >

                        {{--<div class= "carousel-logo col-xs-4 col-sm-3 col-sm-2">
                           {{HTML::image('img/logo.png','',array('class' => 'img-responsive'))}}
                        </div>
                        <div class= "carousel-title col-sm-10 col-md-7 col-xs-offset-4 col-sm-offset-3 col-sm-offset-2 hidden-xs">
                           <h3>INSTITUTO UNVIERSITARIO DE TECNOLOGIA <br> "Dr Federico Rivero Palacio"</h3>
                        </div>--}}
                        @foreach ($dir as $file)
                            @if(in_array(strtolower($file->getExt()),$supported_images))
                               <div class="item {{($i++==0)?'active':''}}">
                                    <a href="{{$file->link}}"> 
                                        {{HTML::image($file->imgPath)}}
                                    </a>
                                    <div class="carousel-caption">
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <!-- Controles -->
                        <a href="#carouselIUT" class="left carousel-control" role="button" data-slide="prev">
                           <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                        <a href="#carouselIUT" class="right carousel-control" role="button" data-slide="next">
                            <i class="glyphicon glyphicon-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
     @else
        <div id="ministerioLink"></div>
        <div id="ministerioCarousel"></div>
    @endif

<!-- END|CAROUSEL -->
    <div class="row">
        <h2> Noticias </h2>
        @include('news/newLists')
    </div>
    <hr>
    <div class="row">
        <h2> Multimedia </h2>
        
       @foreach ($multimedia as $key)
        <div class="row">   
            <div class="col-sm-12">
                @if (!($key->ext == 'webm' || $key->ext == 'mp4'))
                {{HTML::image($key->path,'',array('class' => 'img-responsive','style' => 'max-width:100%;height:auto;'))}}
                @else
                <video controls class="img-responsive border" style="max-width:100%;height:auto;">
                <source src="{{URL::to($key->path)}}" type="video/webm"></source>
                </video>
                @endif
                <h5>{{$key->description}}</h5>
            </div>
        </div>
        <hr>
        @endforeach
        
    </div>
    @if(file_exists('img/indexPhoto/display-1.png'))
    <!-- Modal Añadir Usuario -->
        <div class="modal fade" id="modalPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
            <div class="modal-dialog" role="document" style="width: initial; max-width:80%">
                <div class="modal-content">
                    <div class="modal-body">
                        <center>
                        {{(@$photoIndexURL<>'#')? "<a href='$photoIndexURL' target='_blank'>":""}}
                        
                           <img src="{{URL::to('img/indexPhoto/display-1.png?'.rand())}}" 
                                alt="Ir a {{@$photoIndexURL}}" 
                                class="img-responsive"
                                style="margin:0;"
                                >
                         {{(@$photoIndexURL<>'#')?'</a>':''}}
                        </center>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('js')
<script>
    $(document).ready(function(){
        
        $('#modalPhoto').modal('show');
         $('#ministerioCarousel').load('ministerio.html .view.view-nodequeue-2.view-id-nodequeue_2');
    });

</script>
@stop