@extends('admin.layoutAdmin')

@section('title') Gestionar Multimedia @stop

@section('content')



		<div class="row">

		<button class="btn btn-info" title="agregar" data-toggle="modal" data-target="#addMultimedia"> 
			<i class="glyphicon glyphicon-plus-sign"></i> Agregar multimedia

		</button>
		</div>
		<hr>


		@foreach ($multimedia as $key)
		<div class="row">	
			<div class="col-sm-6">
				{{--*/$host  = $_SERVER['HTTP_HOST'];
				$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
				$base = substr("http://". $host . $uri,0,-15).$key->path;/*--}}
				@if (!($key->ext == 'webm' || $key->ext == 'mp4' || $key->ext == 'pdf'))
				{{HTML::image($key->path,'',array('class' => 'img-responsive'))}}
				@elseif (($key->ext == 'pdf'))
				<embed src="{{$base}}" width='490' height='375'>
				@else
				<video controls class="img-responsive border" style="max-width:100%;height:auto;">
                <source src="{{$base}}" type="video/mp4"></source>
           		</video>
				@endif

			</div>
			<div class="col-sm-6">
				<button class="btn btn-warning" title="modificar" data-toggle="modal" data-target="#changeMultimedia{{$key->id}}"> 
				Modificar
				</button>
				<button class="btn btn-danger" title="Eliminar" data-toggle="modal" data-target="#deleteMultimedia{{$key->id}}"> Eliminar </button>
				<br><br>
				<h2>
				{{$key->description}}
				</h2>
			</div>
		</div>

		<div class="modal fade" id="deleteMultimedia{{$key->id}}" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Realmente deseas eliminar ésta multimedia?</p>
                    </div>
                    <div class="modal-footer">
                        {{Form::open(array('url' => URL::to('admin/multimedia/'.$key->id), 'method' => 'post','files' => true ,'class' => 'form-horizontal'))}}
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

                        {{Form::submit('Eliminar',array('class' => 'btn btn-danger'))}}
                        {{Form::close()}}
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


		<hr>
		

	<!-- Modal para Modificar -->
	<div class="modal fade" id="changeMultimedia{{$key->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Modificar</h4>
                    </div>
				<!-- Cuerpo del Modal -->
				<div class="modal-body">
				{{Form::open(array('url' => URL::to('admin/multimedia'), 'method' => 'post','files' => true ,'class' => 'form-horizontal'))}}
				    <div class="form-group">
					 	{{Form::label('archivo', 'Multimedia:', array('class' =>'col-sm-4 control-label'))}}
					 	<div class="col-sm-8 ">
    						{{Form::file('archivo', array('class'=>'form-control '))}}
					 	</div>
					</div>

					<div class="form-group">
						{{Form::label('description', 'Descripci&oacute;n:', array('class' =>'col-sm-4 control-label'))}}
						<div class="col-sm-8">
							{{Form::text('description',$key->description,array('placeholder' => 'Estoy describiendo a la imagen o video', 'class' => 'form-control','maxlength' => 14))}}
						</div>
					</div>
				</div>
				<!-- Botones -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					{{Form::submit('Enviar',array('class' => 'btn btn-warning'))}}
					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>

		@endforeach
		{{$multimedia->links()}}

	<!-- Modal para Agregar -->
	<div class="modal fade" id="addMultimedia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<!-- Encabezado -->
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"> Agregar Multimedia </h4>
				</div>

				<!-- Cuerpo del Modal -->
				<div class="modal-body">
				{{Form::open(array('url' => URL::to('admin/multimedia'), 'method' => 'post','files' => true ,'class' => 'form-horizontal'))}}
				    <div class="form-group">
					 	{{Form::label('archivo', 'Multimedia:', array('class' =>'col-sm-4 control-label'))}}
					 	<div class="col-sm-8 ">
    						{{Form::file('archivo', array('class'=>'form-control '))}}
					 	</div>
					</div>

					<div class="form-group">
						{{Form::label('description', 'Descripci&oacute;n:', array('class' =>'col-sm-4 control-label'))}}
						<div class="col-sm-8">
							{{Form::text('description','',array('placeholder' => 'Estoy describiendo a la imagen o video', 'class' => 'form-control','maxlength' => 14))}}
						</div>
					</div>
				</div>

				<!-- Botones -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					{{Form::submit('Enviar',array('class' => 'btn btn-info'))}}
					{{Form::close()}}
				</div>
			</div>
		</div>
	
@stop