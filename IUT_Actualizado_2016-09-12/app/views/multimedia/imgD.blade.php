<div class="col-sm-6">
	{{$key->path}}
	@if (!($key->ext == 'webm' || $key->ext == 'mp4'))
		{{HTML::image($key->path,'',array('class' => 'img-responsive'))}}
	@else
		<video controls class="img-responsive border" style="width:100%;height:auto;">
	    	<source src="{{URL::to($key->path)}}" type="video/webm"></source>
	    </video>
	@endif
	<h5>{{$key->description}}</h5>
</div>