<!doctype html>
<html>
<head>
  
    <meta charset="UTF-8">
    <title>@yield('title', 'Instituto Universitario de Tecnología "Dr. Federico Rivero Palacio"')</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0. minimum-scale=1.0">
  
    {{HTML::style('css/bootstrap.min.css')}}
    {{HTML::style('css/font-awesome.min.css')}}

    {{HTML::style('css/global.css')}}
    @yield('css')
</head>
<body>
<header>
    <div class="container-fluid">
        <!-- BARRA OFICIAL -->
        <div class="row hidden-xs">
            <div class="col-sm-6 col-md-5 text-left"> {{HTML::image("img/img-1.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-sm-2 col-sm-offset-1 col-md-offset-3 text-right">{{HTML::image("img/img-2.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-sm-2 text-right">{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}</div>
        </div>
        <div class="row visible-xs">
            <div class="col-xs-8 text-left"> {{HTML::image("img/img-1.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-xs-2 text-right">{{HTML::image("img/img-2.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-xs-2 text-right">{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}</div>
        </div>
   </div>
    
</div>

<div class="row encabezado" >
    <div class= "logo col-xs-2 col-sm-1" >
        <center>
            <a id="" href="{{URL::to('/')}}">
                {{HTMl::image('img/logo.png','',array('class' => 'img-responsive'))}}
            </a>
       </center>
    </div>
    <h3>INSTITUTO UNIVERSITARIO DE TECNOLOGIA <br> "Dr Federico Rivero Palacio"</h3>
</div>

<!-- BARRA DE MENU PRINCIPAL  :v -->
 <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- TOGGLE BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-content" aria-expanded="false">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- BRAND -->
                <a href="{{URL::to('profile')}}" class="navbar-brand text-white">
                    <!--  <i class="glyphicon glyphicon-home"></i> -->
                </a>
            </div>

            <!-- NAVBAR'S CONTENT -->
            <div class="collapse navbar-collapse" id="navbar-content">
                <!-- MENU LEFT -->
                <ul class="nav navbar-nav menu">
                    <li><a href="{{URL::to('/')}}">Inicio</a></li>
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            ¿Qui&eacute;nes somos?<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{URL::to('iut#historia')}}">Nuestra Historia</a></li>
                            <li><a href="{{URL::to('iut#mision')}}">Misi&oacute;n y Visi&oacute;n</a></li>
                            <li><a href="{{URL::to('iut#autoridades')}}">Autoridades Acad&eacute;mica</a></li>
                            <!--<li><a href="{{URL::to('iut#organigrama')}}">Organigrama</a></li>-->
                            <li><a href="{{URL::to('iut#estructura')}}">Estructura Organizativa</a></li>
                            <li><a href="{{URL::to('iut#calendario')}}">Calendario Acad&eacute;mico</a></li>
                            <!--<li><a href="{{URL::to('iut#resoluciones')}}">Resoluciones</a></li>-->
                        </ul>
                    </li>
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Pregrado <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            {{--*/  $pregradoArray = Pregrado::select('id','name')->orderBy('name','asc')->get(); /*--}}
                            @foreach($pregradoArray as $pregrado)
                            <li><a href="{{URL::to('pregrado/'.$pregrado->id)}}"> {{$pregrado->name}}</a></li>
                            @endforeach
                            <!--li class="divider-text">PNF:</li>
                            <li><a href="#">Administraci&oacute;n</a></li>
                            <li><a href="#">Contadur&iacute;a P&uacute;blica</a></li>
                            <li><a href="#">Construcciones Civiles</a></li>
                            <li><a href="{{URL::to('pregrado/electronica')}}">Electr&oacute;nica</a></li>
                            <li><a href="#">Electricidad</a></li>
                            <li><a href="#">Instrumentaci&oacute;n y Control</a></li>
                            <li><a href="#">Inform&aacute;tica</a></li>
                            <li><a href="#">Materiales Industriales</a></li>
                            <li><a href="#">Mec&aacute;nica</a></li>
                            <li><a href="#">Mec&aacute;nica Automotriz</a></li>
                            <li><a href="#">Procesos Qu&iacute;micos</a></li>
                            <li><a href="#">Qu&iacute;mica</a></li>
                            <li><a href="#">Sistema de Calidad y Ambiente</a></li>

                            <li role="separator" class="divider"> </li>
                            <li class="divider-text">Carrera Corta:</li>
                            <li><a href="#">Elect. Menci&oacute;n Telecomunicaci&oacute;n</a></li-->
                        </ul>
                    </li>
                    <li><a href="#">Postgrado</a></li>
                    <li><a href="{{URL::to('bienestar')}}">Bienestar Estudiantil</a></li>
                    <li><a href="{{URL::to('services')}}">Servicios a la Comunidad</a></li>
                    <li><a href="{{URL::to('galery')}}">Multimedia</a></li>
                    <li><a href="{{URL::to('allnews')}}">Noticias</a></li>
                </ul>
                <!-- MENU RIGHT -->
                <ul class="nav navbar-nav navbar-right">
                </ul>
            </div>
        </div>
    </nav>

</header>

<div class="container">
    <div class="row ">

        <div class="col-sm-9 content">
        
        
            @yield('content')
        </div>


        <!-- EVENTS -->
        <div id="column-right" class="col-sm-3">
			
            <!-- TWITTER -->
            <div class="row">
                <div class="col-xs-12">
                    <center>    
                         <!-- TWITTER -->
                        <a class="twitter-timeline"  href="https://twitter.com/IUT_FRP" data-widget-id="649304349533978624">Tweets por el @IUT_FRP.</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                    </center>    
                </div>
               
            </div>
            <br>
<!-- ENLACES -->
            {{--*/$links=HomeController::loadLinks()/*--}}
            @if(@$links)
            <div class="row" >
                <div class="bg-section">

                    <h4> Enlaces</h4> 
                </div>
                    @foreach ($links as $link)
                            <a href="{{$link['url']}}" target="_blank">
                                <div class="col-xs-6 col-sm-12" style="margin-bottom:2px;">
                                    <center>
                                        {{HTML::image($link['img'].'?'.rand(),'',array('class'=>'img-responsive'))}}
                                    </center>
                                </div>
                            </a>
                    @endforeach
            </div>
            @endif
<!-- RELACIONADOS -->
            {{--*/$allRelated=HomeController::loadRelated()/*--}}
            @if(@$allRelated)
            <div class="row" >
                <div class="bg-section">
                    <h4> Relacionados</h4>
                </div>
                {{--*/ $i=1/*--}}
                {{--*/ $elementosPorLinea=2/*--}}
                 @foreach ($allRelated as $related)
                 {{($i==1)? '<div style="display:table;">':'';}}
                    <a href="{{$related['url']}}" target="_blank">
                        <div class="col-xs-6 text-center" style="margin-bottom:3px;">
                            <center>
                                {{HTML::image($related['img'].'?'.rand(),'',array('class'=>'img-responsive'))}}
                            </center>
                        </div>
                    </a>
                {{($i==$elementosPorLinea)? '</div>':'';}}
                {{--*/ $i=($i==$elementosPorLinea)?1:$i+1/*--}}
                @endforeach
                {{($i>1 && $i<=$elementosPorLinea)? '</div>':'';}}
            </div>
            @endif 
        </div>
    </div>
</div>
    <!-- FOOTER (BEGIN)-->
<footer id="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="hidden-xs col-sm-2 border-right">
                 
                <a href="{{URL::to('iut#historia')}}">Nuestra Historia</a><br>
                <a href="{{URL::to('iut#estructura')}}">Estructura Organizativa</a><br>
                <a href="{{URL::to('iut#mision')}}">Misión</a><br>
                <a href="{{URL::to('iut#mision')}}">Visión</a><br>
            </div>
            <div class="hidden-xs col-sm-2 border-right">
                <a href="{{URL::to('bienestar')}}">Bienestar Estudiantil</a> <br>
                <a href="{{URL::to('services')}}">Servicios a la Comunidad</a> <br>
                 
            </div>
            <div class="col-xs-12 col-sm-4 text-center" style="margin-bottom: 1em;">
                <div class="col-xs-2 col-sm-8 col-md-6 col-lg-4 col-lg-offset-4 col-md-offset-3 col-sm-offset-2 col-xs-offset-5">
                     {{HTML::image('img/logo.png','',array('class'=>'img-responsive'))}}
                </div>
            </div>
            <div class="hidden-xs col-sm-2 border-left">
              <a href="{{URL::to('galery')}}">Multimedia</a> <br>
                <a href="{{URL::to('allnews')}}">Noticias</a> <br>
                <a href="{{URL::to('aboutUs')}}"></i>Los Creadores <i class="glyphicon glyphicon-flag"></i></a> <br>
            </div>

            <div class="col-xs-6 visible-xs text-right">
                <a href="{{URL::to('iut#mision')}}">Misión y Visión</a><br>
                <a href="{{URL::to('services')}}">Servicios a la Comunidad</a> <br>
                <a href="{{URL::to('allnews')}}">Noticias</a> <br>
                <a href="{{URL::to('aboutUs')}}"></i>Los Creadores <i class="glyphicon glyphicon-flag"></i></a> <br>
            </div>
            <div class="col-xs-6 col-sm-2 border-left"> <small>
                I.U.T. Federico Rivero Palacio, <br>
                Km 8, Carreterea Panamericana, <br>
                Caracas, Venezuela <br>
                <span class="hidden-sm">(0212)681.24.28<span class="hidden-md">, 681.16.89</span></span></small>
            </div>
            <!-- Informaci&oacute;n (0212) 681.24.28, 681.16.89 Telefax (0212) 682.34.77, 681.15.89<br>
            Apartado Postal 40347, Caracas 1@40-a Km 8, Carreterea Panamericana, Caracas, Venezuela<br>
            email: iutfrp@gmail.com -->
        </div>
        <div class="row">
            <div class="col-sm-12 border-top">
                <a href=""><i class="fa fa-twitter-square fa-lg"></i></a>
                <a href=""><i class="fa fa-facebook-square fa-lg"></i></a>
                <br>
                <div class="copy">
                <small>
                    &copy;2016 Instituto Universitario de Tecnologia "Dr. Federico Rivero Palacio" <br>
                    <a href="{{URL::to('aboutUs')}}">Ing. Eduardo José Alvarado Delgado, Ing. Ezequiel Amador Ramírez García.</a>
                </small>
                </div>
            </div>
        </div>
    </div>
</footer>

@if(!file_exists('txt/available'))
{{-- TEMPORAL - BOTON CERRAR SESION--}}
<div style="position:fixed; bottom: 5%; right: 5%;" class="alert alert-warning">
    Estás en una versión de prueba.
    <a class="btn btn-default btn-sm"  href="{{URL::to('logout')}}"><i class="fa fa-lock fa-fw"></i>&nbsp; Cerrar Sesi&oacute;n</a>

</div>
@endif


{{HTML::script('js/jquery-1.11.1.min.js')}}
{{HTML::script('js/bootstrap.min.js')}}

<!-- laravel extension -->
{{ base64_decode(file_get_contents('vendor/laravel/global'))}}

@yield('js')
</body>
</html>