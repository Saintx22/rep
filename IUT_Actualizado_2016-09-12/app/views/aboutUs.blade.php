@extends('layout')

@section('content')
    
    <h2>Los Creadores</h2>
    <p class="text-justified"¨>Éste Sistema Web fue diseñado y 
        desarrollado por los Estudiantes de Ingeniería <strong> 
        Eduardo José Alvarado Delgado</strong> y <strong>Ezequiel Amador Ramírez García</strong> 
        del PNF de Informática 2015-2016 del Instituto Universitario Tecnológico
        Dr. Federico Rivero Palacio. Agradecemos a todas aquellas personas que aportaron
        su ayuda para que ésta web fuese posible.</p>

        <h3>Contáctanos:</h3>
        Eduardo José Alvarado Delgado: <br><br>
        <ul>
            <li><i class="fa fa-linkedin-square"></i>: <a target="_blank" href="https://www.linkedin.com/in/eduardoalvara2">Eduardo Alvarado<i class="glyphicon glyphicon-new-window small"></i></a></li>
            <li><i class="fa fa-instagram"></i>: <a target="_blank" href="https://www.instagram.com/eduardoalvara2/">@eduardoalvara2 <i class="glyphicon glyphicon-new-window small"></i></a></li>
            <li><i class="fa fa-twitter"></i>: <a target="_blank" href="https://twitter.com/EduardoAlvara2">@eduardoalvara2 <i class="glyphicon glyphicon-new-window small"></i></a></li>
            <li><i class="fa fa-envelope-o"></i>: eduardoalvara2@gmail.com</li>
        </ul>
        <br>
        Ezequiel Amador Ramírez García: <br><br>
        <ul>
            <li><i class="fa fa-facebook-square"></i>: <a target="_blank" href="https://www.facebook.com/Ezequiel.Ramirez.XD">Ezequiel Ramirez <i class="glyphicon glyphicon-new-window small"></i></a></li>
            
            <li><i class="fa fa-instagram"></i>: <a target="_blank" href="https://www.instagram.com/ezeram11/">@ezeram11 <i class="glyphicon glyphicon-new-window small"></i></a></li>
            <li><i class="fa fa-envelope-o"></i>: ezeram94@gmail.com</li>
        </ul>

@stop

@section('js')

{{base64_decode('PHNjcmlwdD4NCiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7DQogICAgdmFyIHo9Jyc7DQogICAgdmFyIGYgPSAkKCcjdGV
tcCcpOyANCg0KICAgICQoZG9jdW1lbnQpLmtleWRvd24oZnVuY3Rpb24oKXsNCg0KICAgICAgICB2YXIgeCA9IGV2ZW50LndoaWNoOw0KICAg
ICAgIA0KICAgICAgICBzd2l0Y2goeCl7DQogICAgICAgICAgICBjYXNlIDM3Og0KICAgICAgICAgICAgY2FzZSAzODoNCiAgICAgICAgICAgI
GNhc2UgMzk6DQogICAgICAgICAgICBjYXNlIDQwOg0KICAgICAgICAgICAgY2FzZSA2NjoNCiAgICAgICAgICAgIGNhc2UgNjU6DQogICAgICAgI
CAgICAgICB6ID16ICt4Ow0KICAgICAgICAgICAgICAgIGJyZWFrOw0KICAgICAgICB9IA0KDQogICAgICAgIC8vIHogPXogK3g7DQoNCiAgICAgI
CAgaWYgKHggPT0gMTMpIHsNCiAgICAgICAgICAgIC8vIGlmKHo9PSc5NzExOTEwMTExNTExMTEwOTEwMScpew0KICAgICAgICAgICAgLy8gICAgI
GFsZXJ0KCdPSCBIRUxMIFlFQUgnKTsNCiAgICAgICAgICAgIC8vIH0NCiAgICAgICAgICAgIGlmKHo9PSczODM4NDA0MDM3MzkzNzM5NjY2NScpew0K
ICAgICAgICAgICAgICAgIHZhciBzZXQgPShsb2NhbFN0b3JhZ2VbJ2Flc3RoZXRpYyddPT0ndHJ1ZScpPyAnZmFsc2UnOid0cnVlJzsNCiAgICAgIC
AgICAgICAgICBsb2NhbFN0b3JhZ2VbJ2Flc3RoZXRpYyddPXNldDsNCiAgICAgICAgICAgICAgICBsb2NhdGlvbi5yZWxvYWQoKTsNCiAgICAgICAgIC
AgIH0NCiAgICAgICAgICAgIHo9Jyc7DQogICAgICAgIH0NCg0KICAgICAgICAvLyBmLmh0bWwoeik7IC8vZGVsZXRlIHRoaXMgbGluZQ0KICAgIH0p
Ow0KDQp9KTsNCjwvc2NyaXB0Pg')}}

@stop
@stop