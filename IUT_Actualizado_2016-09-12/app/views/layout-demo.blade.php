<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title', 'Instituto Universitario de Tecnología "Dr. Federico Rivero Palacio"')</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0. minimum-scale=1.0">
    {{HTML::style('css/bootstrap.min.css')}}
    {{HTML::style('css/font-awesome.min.css')}}
    {{HTML::style('css/global.css')}}
    @yield('css')
</head>
<body style="background-color: #3C8BD0;">
<header>
    <div class="container-fluid">
        <!-- BARRA OFICIAL -->
        <div class="row hidden-xs" style="background-color: white;">
            <div class="col-sm-6 col-md-5 text-left"> {{HTML::image("img/img-1.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-sm-2 col-sm-offset-1 col-md-offset-3 text-right">{{HTML::image("img/img-2.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-sm-2 text-right">{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}</div>
        </div>
        <div class="row visible-xs" style="background-color: white;">
            <div class="col-xs-8 text-left"> {{HTML::image("img/img-1.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-xs-2 text-right">{{HTML::image("img/img-2.png",'',array('class'=>'img-responsive'))}}</div>
            <div class="col-xs-2 text-right">{{HTML::image("img/img-3.png",'',array('class'=>'img-responsive'))}}</div>
        </div>
   </div>
    


</div>
    <!-- CAROUSEL -->

   {{--*/ $path= 'img/carousel/' /*--}}
   {{--*/ $dir= scandir($path) /*--}}
   {{--*/ $supported_images = array('gif','jpg','jpeg','png','bmp');/*--}}
   {{--*/ $i=0/*--}}
    <div class="container-fluid" >
        <div class="row">
            <div id="carouselIUT" class="carousel slide" data-ride="carousel" >
                <!-- Indicadores -->

                <ol class="carousel-indicators">
                    @foreach ($dir as $file)
                        @if(in_array(strtolower(pathinfo($file)['extension']),$supported_images))
                            <li data-target="#carouselIUT" class="{{($i==0)?'active':''}}" data-slide-to="{{$i++}}"></li>
                        @endif
                    @endforeach
                    {{--*/ $i=0/*--}}
                </ol>
                
                <!-- Contenedores de slide -->
                <div class="carousel-inner " role="listbox" >

                    <div class= "carousel-logo col-xs-4 col-sm-3 col-sm-2">
                       {{HTMl::image('img/logo.png','',array('class' => 'img-responsive'))}}
                    </div>
                    <div class= "carousel-title col-sm-10 col-md-7 col-xs-offset-4 col-sm-offset-3 col-sm-offset-2 hidden-xs">
                       <h3>INSTITUTO UNIVERSITARIO DE TECNOLOGIA <br> "Dr Federico Rivero Palacio"</h3>
                    </div>
                    @foreach ($dir as $file)
                        @if(in_array(strtolower(pathinfo($file)['extension']),$supported_images))
                           <div class="item {{($i++==0)?'active':''}}">
                                {{HTML::image($path.$file)}}
                                <div class="carousel-caption">
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <!-- Controles -->
                    <a href="#carouselIUT" class="left carousel-control" role="button" data-slide="prev">
                        
                    </a>
                    <a href="#carouselIUT" class="right carousel-control" role="button" data-slide="next">
                        
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container-fluid" style=" color: #FFF;">
    <div class="row ">
        <div class="col-sm-10 col-sm-offset-1" >
            @yield('content')
        </div>
        
    </div>
</div>

{{HTML::script('js/jquery-1.11.1.min.js')}}
{{HTML::script('js/bootstrap.min.js')}}
{{HTML::script('js/relacionAspecto.js')}}
@yield('js')
</body>
</html>