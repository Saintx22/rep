<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/*
Route::get('/', function(){
    return View::make('index-demo');
});
*/

Route::get('/*', function()
{
    return Redirect::to('login');
});

Route::get('/', 'HomeController@showIndexDemo'); 
Route::get('/index-demo', 'HomeController@showIndexDemo'); 

Route::get('admin',function(){return Redirect::to('login');});
Route::get('aboutUs','HomeController@aboutUs');

Route::group(array('prefix' => 'login'),function(){ //PREFIX LOGIN
    Route::get('','UserController@ShowLogin');
    Route::post('','UserController@doLogin');
});



// ROUTES ACCES BY GENERAL PUBLIC WHEN WEB IT
if (file_exists('txt/available')||Auth::check())
{

	//Home Route
	Route::get('/', 'HomeController@showIndex');

	//About IUT Route
	Route::get('iut', function(){return View::make('iut');});

	//New's Route
	Route::get('allnews/','NewsController@showAllNews');
    Route::get('news/{id}','NewsController@showNews');
    Route::get('news/version/{codename}/{version}','NewsController@showNewsVersion');

    //Pregrados's Routes ------------------------
    Route::get('pregrado/{carreer}','PregradoController@ShowPregrado');
    Route::get('pregrado/{carreer}/info/{info}','HomeController@ShowCarreerInfo');
    Route::get('download/pdf/{id}','PregradoController@findPDF');

    //Multimedia's Routes --------
    Route::get('galery','MultimediaController@showMultimedia');
    Route::get('download/img/{path}','MultimediaController@findImg');
    Route::get('download/multimedia/{id}','MultimediaController@downloadMultimedia');
    
    // Others -----
    Route::get('bienestar','HomeController@showBienestar');
    Route::get('services','HomeController@showServices');

}


if(Auth::check()){
// OLD AUTH CHECK POSITION BEGIN <<
    Route::group(array('prefix' => 'admin'),function(){ //PREFIX ADMIN
        Route::get('/','AdminController@showAdminMenu');

        //CHANGE OFFICIAL IMAGES
        Route::get('topBarChange','AdminController@showtopBarChange');
        Route::post('topBarChange','AdminController@topBarChange');
		
		//MANAGE USERS
        Route::get('users','UserController@showUsers');
        Route::get('newUser','UserController@showAddUser');
        Route::post('newUser','UserController@addUser');
        Route::get('user/delete/{id}','UserController@deleteUser');
        Route::get('user/restore/{id}','UserController@restorePassword');
        Route::match(array('GET', 'POST'),'user/changeRol/{id}','UserController@changeRol');
        Route::get('user/changePassword','UserController@changePasswordShow');
        Route::post('user/changePassword','UserController@changePassword');
        Route::get('user/changePorfile','UserController@changePorfileShow');
        Route::post('user/changePorfile','UserController@changePorfile');

 		//MANAGE HTML (BIENESTAR, SERVICIOS, QUIENES SOMOS)
        Route::get('html','AdminController@showAllHtml');
        Route::get('html/{name}','AdminController@showEditHtml');
        Route::post('html/{name}','AdminController@EditHtml');



        //CHANGE CAROUSEL IMAGES
        Route::get('carouselEdit','CarouselController@showCarouselEdit');
        Route::get('newPicture','CarouselController@showNewPhoto');
        Route::post('newPicture','CarouselController@addNewPhoto');
        Route::get('eliminar/{eliminar}','CarouselController@deletePhoto');
        Route::post('switchCarousel','CarouselController@switchCarousel');

        //Manage Multimedia
        Route::get('multimedia', 'MultimediaController@manageMultimedia');
        Route::post('multimedia', 'MultimediaController@addMultimedia');
        Route::post('multimedia/{id}', 'MultimediaController@deleteMultimedia');

        // indexPhoto (Image Alert)
        Route::group(array('prefix' => 'indexPhoto'),function(){ //PREFIX ADMIN
            Route::get('/','AdminController@showIndexPhoto');
            Route::post('Upload','AdminController@indexPhotoUpload');
            Route::post('SetDisplay','AdminController@indexPhotoSetDisplay');
            Route::post('SetURL','AdminController@indexPhotoSetURL');
            Route::get('UnsetURL','AdminController@indexPhotoUnsetURL');
        });

        // Manage Links
        Route::get('links','AdminController@showLinks');
        Route::post('addLink','AdminController@addLink');
        Route::post('deleteLink','AdminController@deleteLink');
        
        // Manage Related
        Route::get('related','AdminController@showRelated');
        Route::post('addRelated','AdminController@addRelated');
        Route::post('deleteRelated','AdminController@deleteRelated');

        Route::post('available','AdminController@available');
    });

    //New's Routes AUTH------------------------
    Route::get('searchNews', 'NewsController@searchNews');
    Route::get('adminNews', 'NewsController@showAdminNews');
    Route::get('deleteNews/{eliminar}','NewsController@deleteNews');

    Route::get('addNews', 'NewsController@showAddNew');
    Route::post('addNews', 'NewsController@addNews');

    Route::get('changeNews/{id}', 'NewsController@showChangeNew');
    Route::post('changeNews/{id}', 'NewsController@changeNews');

    //Pregrados's Routes AUTH------------------------
    Route::get('searchPregrado', 'PregradoController@searchPregrado');
    Route::get('adminPregrado', 'PregradoController@showAdminPregrado');
    Route::get('deletePregrado/{id}','PregradoController@deletePregrado');

    Route::get('addPregrado', 'PregradoController@showAddPregrado');
    Route::post('addPregrado', 'PregradoController@addPregrado');

   // Route::get('modifyPregrado/{id}', 'PregradoController@showModifyPregrado');
    //Route::post('modifyPregrado/{id}', 'PregradoController@modifyPregrado');

    Route::get('changePregrado/{id}','PregradoController@showChangePregrado' );
    Route::post('changePregrado/{id}','PregradoController@changePregrado' );
	// OLD AUTH CHECK POSITION END <<

}

 Route::get('logout','UserController@doLogout');




 Route::filter('main', function(){if (!file_exists('txt/available')&&!Auth::check())return Redirect::to('/');});