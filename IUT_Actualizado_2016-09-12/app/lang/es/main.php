<?php
/**
 * Global properties
*
*/
return array(

    'app_fullname' => 'Interfaces de usuario para qu&iacute;mica computacional',
    'version'      => 'versi&oacute;n',
    'submit'       => 'Enviar',
    'app_about'    => 'Nosotros',
    'app_support'  => 'Soporte',
    'app_community'     => 'Comunidad',
    'app_terms&privacy' => 'Terminos y Privacidad',
    'language'     => 'Idioma',
		'languages'    => array(
				'en'   => 'Ingl&eacute;s',
				'es'   => 'Espa&ntilde;ol',
		),
    
    // SANDBOX
    // labels
    'label_search' => 'Buscar',
    
    // Notifications
    'new_notifications' => 'Tienes :number notificaciones',    
);
