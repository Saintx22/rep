<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Pregrado extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pregrado';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $fillable = array('id','name','contentPath');
    /**
    * Add News (Añadir Noticia).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return functions Redirect::to, MessageBag, Inputs values, boolean true.
    *
    */
    public static function addPregrado(){
        $name   = Input::get('name');
        $content = Input::get('content');

        // Array with all the data that will be added/updated
        $data=array(
            'name'    => $name,
            'content'  => $content,
        );

        // Validator's rules
        $rules=array(
            'name'    => 'required|min:5|max:256',
            'content'  => 'required|min:200',
        );

        // Validate the data according the rules given
        $validator = Validator::make($data, $rules);

        // If the validation fails, return the message's bag given by the validator
        if ($validator->fails())
        {
           return Redirect::to('addPregrado')->withErrors($validator)->withInput();
        }

        // If the validation pass, continue to add/update information to the DB. 
        
        // $newsCodename = News::generateCodename($title);

    
        $path='txt/pregrado/';

        //change the array to save
        unset($data['content']); //remove "img"
        $data['contentPath'] = ''; //add "imgPath"
       
        
        $pregrado = new Pregrado($data);
        if($pregrado->save())
        {
            //If was saved successfully.
            


            $pregrado->contentPath = $path.$pregrado->id.'.txt';

            File::put($pregrado->contentPath, $content);

            $pregrado->save();

            return Redirect::to('searchPregrado')->withMessages(array('¡Pregrado Añadido!'));
            
                   
        }

         
       
   }

   /**
    * Generate the news's codename(Genera el nombre código de la noticia).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return String.
    *
    */
   public static function generateCodename($title){
        date_default_timezone_set('America/Caracas');
        return 'n'.date('ymdHis').substr($title, 0, 5); //y=year;m=month;d=day;H=hour(24);i=minute;s=seconds
   }
   
    

   public static function getPregrado($id){

        $pregrado = Pregrado::find($id);

        return $pregrado;
   }
 //MODIFICAR PREGRADO 
    public static function changePregrado($id){
        $name   = Input::get('name');
        $contentPath = Input::get('contentPath');

        // Array with all the data that will be added/updated
        $data=array(
            'name'    => $name,
            'contentPath'  => $contentPath,
        );

        // Validator's rules
        $rules=array(
            'name'    => 'required|min:5|max:256',
            'contentPath'  => 'required|min:200',
        );

        // Validate the data according the rules given
        $validator = Validator::make($data, $rules);

        // If the validation fails, return the message's bag given by the validator
        if ($validator->fails())
        {
           return Redirect::to('changePregrado')->withErrors($validator)->withInput();
        }
        // If the validation pass, continue to add/update information to the DB. 
        
        // $newsCodename = News::generateCodename($title);

    
        $path='txt/pregrado/';

        $pregrado = Pregrado::find($id);


            if ($data['name']  !=null)  {$pregrado->name  =$data['name'];}
            if ($data['contentPath'] !=null)  {$pregrado->contentPath =$data['contentPath'];}
        if($pregrado->save())
        {
            //If was saved successfully.
            


            $pregrado->contentPath = $path.$pregrado->id.'.txt';

            File::put($pregrado->contentPath, $contentPath);

            $pregrado->save();

            return Redirect::to('searchPregrado')->withMessages(array('¡Pregrado Añadido!'));
            
                   
        }

         
       
   }

}