<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
*-----------------------------------------------------------
* User's Model (Modelo de Noticias).
*-----------------------------------------------------------
*
* @since August 9, 2015.
*
* @author Eduardo Alvarado <eduardoalvara2@gmail.com>
*
*/

class User extends \Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The database fields used by the model.
     *
     * @var array
     */
    protected $fillable= array('id','ci','type','user','name','last_name','direction','email','password');
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
    * Add User.
    *-----------------------------------------------------------
    *
    * @since March 23, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
    public static function add($data){

        $messages = array(
            'user.unique' => 'El :attribute "'.$data['user'].'" ya existe',
        );

        //Initialize the array that will be returned
        $result = array(
                    'success'=>false,
                    'messages'=>null,
                    );

        //Laravel validator rules
        $rules=array(
            'ci'                 => 'required|integer|min:1000|max:999999999', 
            'user'               => 'required|min:4|max:32|unique:user,user',  
            'type'               => 'required|min:8|max:29|', 
            'name'               => 'required|min:3|max:11', 
            'last_name'          => 'required|min:2|max:11', 
            'direction'          => 'required|min:10|max:250', 
            'email'              => 'email|max:128',
            'password'           => 'required|min:8|max:16',
            'passwordConfirm'    => 'required|same:password',
        );

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails())
        {
            $result['messages']= $validator->getMessageBag();
            
        } else {
            //Try yo save de new User.
            $user = new User($data);

            // Hash the password before save.
            $user->password=Hash::make($data['password']);

            if($user->save()){
                //If the user was saved successfully
                 $result['success']= true;
                 $result['messages']= array('¡Usuario Agregado Exitosamente!');
            }else{
                //If not
                $result['messages']= array('Ocurrió un error al intentar agregar el Usuario.');
                
            }
        }
        return $result;
    }


    /**
    * Add User.
    *-----------------------------------------------------------
    *
    * @since March 23, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
    public static function changePorfile($data){

        $messages = array(
            'user.unique' => 'El :attribute "'.$data['user'].'" ya existe',
        );

        //Initialize the array that will be returned
        $result = array(
                    'success'=>false,
                    'messages'=>null,
                    );

        //Laravel validator rules
        $rules=array(
            'ci'                 => 'integer|min:1000|max:999999999', 
            'user'               => 'min:4|max:32|unique:user,user',  
            'name'               => 'min:3|max:11', 
            'last_name'          => 'min:2|max:11', 
            'direction'          => 'min:10|max:250', 
            'email'              => 'email|max:128',
        );

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails())
        {
            $result['messages']= $validator->getMessageBag();
            
        } else {
            //Try yo save de new User.
            
            $user = User::find(Auth::id());
            if ($data['ci']!=null) {$user->ci=$data['ci'];}
            if ($data['user']!=null) {$user->user=$data['user'];}
            if ($data['name']!=null) {$user->name=$data['name'];}
            if ($data['last_name']!=null) {$user->last_name=$data['last_name'];}
            if ($data['direction']!=null) {$user->direction=$data['direction'];}
            if ($data['email']!=null) {$user->email=$data['email'];}

            if($user->save()){
                //If the user was saved successfully
                 $result['success']= true;
                 $result['messages']= array('¡Usuario Agregado Exitosamente!');
            }else{
                //If not
                $result['messages']= array('Ocurrió un error al intentar agregar el Usuario.');
                
            }
        }
        return $result;
    }
    
    /**
    * Restore Password .
    *-----------------------------------------------------------
    *
    * @since March 23, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *         Ezequiel Ramírez <ezeram94@gmail.com>
    *
    * @return array.
    *
    */

    public static function restorePassword($id){

        //Initialize the array that will be returned
        $result = array('success'=>false,'pass'=>null);
        $randPass = rand(10000000,99999999);
        $User = User::find($id);

        $User->password = Hash::make($randPass);

        if($User->save()){
            $result['success'] = true;
            $result['pass'] = $randPass;
        }

        return $result;
    }

     /**
    * Change Password.
    *-----------------------------------------------------------
    *
    * @since March 27, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
    public static function changePassword($data){

        //Initialize the array that will be returned
        $result = array(
                    'success'=>false,
                    'messages'=>null,
                    );

        //Laravel validator rules
        $rules=array(
            'oldPassword'        => 'required',
            'password'           => 'required|min:8|max:16',
            'passwordConfirm'    => 'required|same:password',
        );

        $validator = Validator::make($data, $rules);

        if ($validator->fails())
        {
            $result['messages']= $validator->messages()->all();
            
        } else {
            //Try yo save de new User.
            $user = User::find(Auth::id());
            if(Hash::check($data['oldPassword'],$user->password)){

                // Hash the password before save.
                $user->password=Hash::make($data['password']);

                if($user->save()){
                    //If the user was saved successfully
                     $result['success']= true;
                     $result['messages']= array('¡Contraseña actualizada Exitosamente!');
                }else{
                    //If not
                    $result['messages']= array('Ocurrió un error al intentar cambiar la Contraseña.');
                }
            }else{
                 $result['messages']= array('Contraseña no coincide.');
            }
        }
        return $result;
    }


    /**
    * Change Password.
    *-----------------------------------------------------------
    *
    * @since March 27, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
    public static function changeRol($data){
        Validator::extend('current', function($attribute, $value, $parameters)
        {
            if (Auth::id()==$value) {
                return false;
            }else{
                return true;
            }
        });

        $messages= array(
            'current' => 'No puedes modificarte a ti mismo.',
        );

        //Laravel validator rules
        $rules=array('id'   => 'current',
                     'type' => 'required|min:8|max:29|',);

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails())
        {
            return array('validator_fails', 'validator' => $validator);
            
        } else {

            $User = User::find($data['id']);
            $User->type = $data['type'];

            if($User->save()){
                return array('success');
            }else{
                return array('delete_fails');
            }

        }
        
    }

    /**
    * Delete User.
    *-----------------------------------------------------------
    *
    * @since April 3, 2016. (Remaked)
    *
    * @author  Ezequiel Ramírez <ezeram94@gmail.com>
    *          Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array.
    *
    */
    public static function deleteUser($data){

        //Add new rule to controller, verified I'm not triying to delete the 
        //user where I'm logged
        Validator::extend('current', function($attribute, $value, $parameters)
        {
            if (Auth::id()==$value) {
                return false;
            }else{
                return true;
            }
        });

        $messages= array(
            'current' => 'No puedes eliminarte a ti mismo.',
        );

        //Laravel validator rules
        $rules=array(
            'id'        => 'current',
        );

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails())
        {
            return array('validator_fails', 'validator' => $validator);
            
        } else {

            $User = User::find($data['id']);

            if($User->delete()){
                return array('success');
            }else{
                return array('delete_fails');
            }

        }
        
    }

    

    
}
