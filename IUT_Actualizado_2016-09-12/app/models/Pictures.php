<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
*-----------------------------------------------------------
* Picture's Model (Modelo de Imagenes).
*-----------------------------------------------------------
*
* Este modelo, consta de las imagenes generales de la pagina.
* @since August 9, 2015.
*
* @author Ezequiel Ramirez <ezeram94@gmail.com>
*
*/

class Pictures extends \Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    protected $table = 'pictures';

    public function getExt(){
    	$array = explode('.', $this->imgPath);
    	$ext =  end($array);
    	return $ext;
    }
 
}
