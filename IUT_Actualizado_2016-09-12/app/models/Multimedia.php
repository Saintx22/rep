<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;


class Multimedia extends \Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;

    protected $table = 'multimedia';

     public function validate($file,$description)
    {
    	$validator = Validator::make(
			 array(
			        'archivo' => $file,
			        'description' => 'Description:'.$description 
			    ),

		    array(
		       		'archivo' => 'required',
			        'description' => 'required'
			    )
    		);
    	if ($validator->fails())
    		return $validator;
    	$this->path = 'multimedia/'.date('l_jS_\of_F_Y_h_i_s_A').'.'.$file->getClientOriginalExtension();  
    	$this->description = $description;
    	$this->ext = $file->getClientOriginalExtension();
    	$this->save();
    	$file->move('multimedia',date('l_jS_\of_F_Y_h_i_s_A').'.'.$file->getClientOriginalExtension());
	 	return null;
    }
/*
     public function validateChange($id,$file,$description)
    {
        $validator = Validator::make(
             array(
                    'archivo' => $file,
                    'description' => 'Description:'.$description 
                )
            );

         $multimedia= multimedia::find($id);
            if ($data['archivo']   !=null)  {$news->title   =$data['title'];}
            if ($data['imgFo'] !=null)  {$news->imgFoot =$data['imgFoot'];}
        if ($validator->fails())
            return $validator;
        $this->path = 'multimedia/'.date('l jS \of F Y h i s A').'.'.$file->getClientOriginalExtension();  
        $this->description = $description;
        $this->ext = $file->getClientOriginalExtension();
        $this->save();
        $file->move('multimedia',date('l jS \of F Y h i s A').'.'.$file->getClientOriginalExtension());
        return null;
    }
*/
 
}
