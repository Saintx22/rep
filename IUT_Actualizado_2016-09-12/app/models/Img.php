<?php

/**
*-----------------------------------------------------------
* Images's Model (Modelo de Imágenes).
*-----------------------------------------------------------
*
* @since June 14, 2015.
*
* @author Eduardo Alvarado <eduardoalvara2@gmail.com>
*
*/

class Img{
    /**
    * Change the an image's format to PNG (Cambia el formato de una imagen a PNG).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return String with error or empty.
    *
    */
    public static function toPng($inputFileImg,$path,$newName=''){ 
    //(Image file, the location where will be saved, new name) 
    //(Archivo tipo imagen, el lugar donde será guardado, nuevo nombre)


        $dir= $path;
        $name=pathinfo($inputFileImg->getClientOriginalName(),  PATHINFO_FILENAME); //I got just the name without the extension
        $ext=$inputFileImg->getClientOriginalExtension();  
        
        if(strtoupper($ext)!='PNG')
        {
            $inputFileImg->move($dir,$name.'.'.$ext);

            $ofilename=$dir.'/'.$name.'.'.$ext;
            if($newName)
            {
                $name=$newName;
            }
            $filename=$dir.'/'.$name.'.png';
            
            switch(strtoupper($ext))
            {
                case('JPG'):
                case('JPEG'): $im=@imagecreatefromjpeg($ofilename); break;
                case('GIF'): $im=@imagecreatefromgif($ofilename); break;
                default: File::delete($ofilename); 
                        return false;
            }
            
            if($im)
            {
                File::delete($ofilename);//I delete the old img
                imagepng($im,$filename); //to create the new img with PNG's format
                imagedestroy($im); //clean the memory
            }
       }else{
            if($newName)
            {
                $name=$newName;
            }
            $inputFileImg->move($dir,$name.'.'.$ext);
       }
       return true;
    }

     /**
    * Actualiza la Imagen de Alerta del Inicio
    *-----------------------------------------------------------
    *
    * @since March 8, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return string.
    *
    */
    public static function indexPhotoUpload($inputName){

        $img = Input::file($inputName);

        if($img)
        {
            $save = Img::toPng($img,'img/indexPhoto','display-1');
            if($save)
            {
                @unlink('img/indexPhoto/display-0.png');
            }
            return $save;
        }
        else
            return false;
    }

    /**
    * Actualiza la configuraciíon de si se mostrará o no la Imagen de Alerta del Inicio
    *-----------------------------------------------------------
    *
    * @since March 8, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return string.
    *
    */
    public static function indexPhotoSetDisplay($show){

         $showNegative = ($show=='1')? '0':'1';
        return  @rename("img/indexPhoto/display-$showNegative.png","img/indexPhoto/display-$show.png");

    }

    /**
    * Actualiza la URL de la imágen de alerta
    *-----------------------------------------------------------
    *
    * @since March 30, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return string.
    *
    */
    public static function indexPhotoSetURL($url=null){
        
        //Validate URL
        if(Validator::make(array('url'=>$url),array('url'=>'required|url'))->fails()){

            return false;
        }

        $file= fopen('txt/photoIndexURL.txt', 'w');
        fputs($file,$url);
        fclose($file);
        
        return true;
    
    }

    /**
    * Remueve URL de la imágen de Alerta
    *-----------------------------------------------------------
    *
    * @since March 30, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return string.
    *
    */
    public static function indexPhotoUnsetURL($url=null){
        $success= true;
        $file= fopen('txt/photoIndexURL.txt', 'w') or die($success=false);
        fputs($file,'#');
        fclose($file);
        
        if($success){
            return true;
        }else{
            return false;
        }
    }
    
    /**
    * Agrega una Imagen como Enlace
    *-----------------------------------------------------------
    *
    * @since March 31, 2016.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return array(string, array=null).
    *
    */
    public static function addImageWithLink($data,$path){

        //Laravel validator rules
        $rules=array(
            'img' => 'required|image', 
            'url' => 'required|url', 
        );

        $validator = Validator::make($data, $rules);

        if ($validator->fails())
        {
            return array('error_validator', 'validator' => $validator->getMessageBag());
            
        } else {

            try{

                $name = $data['url'];
                $name = base64_encode($name); //Encode to Base64
                $extension=$data['img']->getClientOriginalExtension();

                // Guardar
                $data['img']->move($path,  $name.'.'.$extension);
            }catch(Exception $e){
                return array('error_move', $e->getMessage());
            }

            return array('success');

        }
    }

    
}