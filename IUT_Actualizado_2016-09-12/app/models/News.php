<?php


use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;



class News extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait, RemindableTrait;
    use SoftDeletingTrait;

    /* Venturecraft Revisionable  (Audit Log)*/
    use \Venturecraft\Revisionable\RevisionableTrait;

    public static function boot()
    {
        parent::boot();
    }

    protected $revisionEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.
    protected $revisionCreationsEnabled = true;
    protected $keepRevisionOf = array(
        'codename',
        'title',
        'author',
        'deleted_at',
        'is_deleted'
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news';
    protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $fillable = array('id','codename','title','content','version','imgPath','imgFoot','author');
    /**
    * Add News (Añadir Noticia).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return functions Redirect::to, MessageBag, Inputs values, boolean true.
    *
    */
    public static function addNews(){
        $title   = Input::get('title');
        $content = Input::get('content');
        $imgFile = Input::file('img');
        $imgFoot = Input::get('imgFoot');
        $author  = Input::get('author');

        // Array with all the data that will be added/updated
        $data=array(
            'codename' => '',//added later
            'title'    => $title,
            'content'  => $content,
            'img'      => $imgFile,
            'imgFoot'  => $imgFoot,
            'author'   => $author,
        );

        // Validator's rules
        $rules=array(
            'title'    => 'required|min:5|max:256',
            'content'  => 'required|min:200',
            'img'      => 'image',
            'imgFoot'  => 'min:3',
            'author'   => 'required|min:3|max:128',
        );

        // Validate the data according the rules given
        $validator = Validator::make($data, $rules);

        // If the validation fails, return the message's bag given by the validator
        if ($validator->fails())
        {
           return Redirect::to('addNews')->withErrors($validator)->withInput();
        }

        // If the validation pass, continue to add/update information to the DB. 
        
        $newsCodename = News::generateCodename($title);

    
        $path='img/news/';
       
        $name = ($imgFile)?$newsCodename.'.'.$imgFile->getClientOriginalExtension():'';
        
        

        //change the array to save
        unset($data['img']); //remove "img"
        $data['imgPath'] = ($imgFile)?$path.$name:null; //add "imgPath"
        $data['codename'] = $newsCodename; //set the codename
        $data['version'] = 1; //set the codename
        $news = new News($data);
        if($news->save())
        {
            //If was saved successfully.
             
            ($imgFile)?$imgFile->move($path,$name):null;
            return Redirect::to('searchNews')->withMessages(array('¡Noticia Añadida Exitosamente!.'))
                                             ->with('newsAdded',$news->id); 
        }

         
        return 'ERROR';
   }
   /**
    * Add News (Añadir Noticia).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return functions Redirect::to, MessageBag, Inputs values, boolean true.
    *
    */
    public static function changeNews($id){
        $title   = Input::get('title');
        $content = Input::get('content');
        $imgFile = Input::file('img');
        $imgFoot = Input::get('imgFoot');
        $author  = Input::get('author');

        // Array with all the data that will be added/updated
        $data=array(
            'codename' => '',//added later
            'title'    => $title,
            'content'  => $content,
            'img'      => $imgFile,
            'imgFoot'  => $imgFoot,
            'author'   => $author,
        );

        // Validator's rules
        $rules=array(
            'title'    => 'required|min:5|max:256',
            'content'  => 'required|min:200',
            'img'      => 'image',
            'imgFoot'  => 'min:3',
            'author'   => 'required|min:3|max:128',
        );

        // Validate the data according the rules given
        $validator = Validator::make($data, $rules);

        // If the validation fails, return the message's bag given by the validator
        if ($validator->fails())
        {
           return Redirect::to('addNews')->withErrors($validator)->withInput();
        }

        // If the validation pass, continue to add/update information to the DB. 
        
        $newsCodename = News::generateCodename($title);

    
        $path='img/news/';
       
        $name = ($imgFile)?$newsCodename.'.'.$imgFile->getClientOriginalExtension():'';
        
        

        //change the array to save
        unset($data['img']); //remove "img"
        $data['imgPath'] = ($imgFile)?$path.$name:null; //add "imgPath"
         $news= News::find($id);
            if ($data['title']   !=null)  {$news->title   =$data['title'];}
            if ($data['imgFoot'] !=null)  {$news->imgFoot =$data['imgFoot'];}
            if ($data['author']  !=null)  {$news->author  =$data['author'];}
            if ($data['content'] !=null)  {$news->content =$data['content'];}
            if ($data['imgPath'] !=null)  {$news->imgPath =$data['imgPath'];}
            else{$data['imgPath']  = $news->imgPath;}
            $data['version']  =$news->version+1;
            $data['codename'] =$news->codename;
        $news = new News($data);
        if($news->save())
        {
            //If was saved successfully.
             
            ($imgFile)?$imgFile->move($path,$name):null;
            return Redirect::to('searchNews')->withMessages(array('¡Noticia modificada Exitosamente!.'))
                                             ->with('newsAdded',$news->id); 
        }

         
        return 'ERROR';
   }
   /**
    * Generate the news's codename(Genera el nombre código de la noticia).
    *-----------------------------------------------------------
    *
    * @since June 14, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return String.
    *
    */
   public static function generateCodename($title){
        date_default_timezone_set('America/Caracas');
        return 'n'.date('ymdHis').substr($title, 0, 5); //y=year;m=month;d=day;H=hour(24);i=minute;s=seconds
   }
   

    /**
    * Get the news (Obtiene las noticias)
    *-----------------------------------------------------------
    *
    * @since November 29, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return Object.
    *
    */
   public static function getNews($id){

        $news = News::find($id);

        return $news;
   }

   /**
    * Delete news (Eliminar noticia)
    *-----------------------------------------------------------
    *
    * @since November 29, 2015.
    *
    * @author Eduardo Alvarado <eduardoalvara2@gmail.com>
    *
    * @return Object.
    *
    */
   public static function deleteNews($id){

        
        $New = News::find($id);
        if(@$New->delete())
        {
            @unlink($New->imgPath);
            return true;
        }else{
            return false;
        }
   
   }
}