<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Instituto Universitario de Tecnología "Dr. Federico Rivero Palacio"</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0. minimum-scale=1.0">
    <link media="all" type="text/css" rel="stylesheet" href="http://localhost/IUT/public_html/css/bootstrap.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="http://localhost/IUT/public_html/css/font-awesome.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="http://localhost/IUT/public_html/css/global.css">

    </head>
<body style="background-color: #3C8BD0;">
<header>
    <div class="container-fluid">
        <!-- BARRA OFICIAL -->
        <div class="row hidden-xs" style="background-color: white;">
            <div class="col-sm-6 col-md-5 text-left"> <img src="http://localhost/IUT/public_html/img/img-1.png" class="img-responsive" alt=""></div>
            <div class="col-sm-2 col-sm-offset-1 col-md-offset-3 text-right"><img src="http://localhost/IUT/public_html/img/img-2.png" class="img-responsive" alt=""></div>
            <div class="col-sm-2 text-right"><img src="http://localhost/IUT/public_html/img/img-3.png" class="img-responsive" alt=""></div>
        </div>
        <div class="row visible-xs" style="background-color: white;">
            <div class="col-xs-8 text-left"> <img src="http://localhost/IUT/public_html/img/img-1.png" class="img-responsive" alt=""></div>
            <div class="col-xs-2 text-right"><img src="http://localhost/IUT/public_html/img/img-2.png" class="img-responsive" alt=""></div>
            <div class="col-xs-2 text-right"><img src="http://localhost/IUT/public_html/img/img-3.png" class="img-responsive" alt=""></div>
        </div>
   </div>
    


</div>
    <!-- CAROUSEL -->

                <div class="container-fluid" >
        <div class="row">
            <div id="carouselIUT" class="carousel slide" data-ride="carousel" >
                <!-- Indicadores -->

                <ol class="carousel-indicators">
                                                                                                                                                                <li data-target="#carouselIUT" class="active" data-slide-to="0"></li>
                                                                                                <li data-target="#carouselIUT" class="" data-slide-to="1"></li>
                                                                                                <li data-target="#carouselIUT" class="" data-slide-to="2"></li>
                                                                                </ol>
                
                <!-- Contenedores de slide -->
                <div class="carousel-inner " role="listbox" >

                    <div class= "carousel-logo col-xs-4 col-sm-3 col-sm-2">
                       <img src="http://localhost/IUT/public_html/img/logo.png" class="img-responsive" alt="">
                    </div>
                    <div class= "carousel-title col-sm-10 col-md-7 col-xs-offset-4 col-sm-offset-3 col-sm-offset-2 hidden-xs">
                       <h3>INSTITUTO UNIVERSITARIO DE TECNOLOGIA <br> "Dr Federico Rivero Palacio"</h3>
                    </div>
                                                                                                                                                               <div class="item active">
                                <img src="http://localhost/IUT/public_html/img/carousel/0.jpg">
                                <div class="carousel-caption">
                                </div>
                            </div>
                                                                                               <div class="item ">
                                <img src="http://localhost/IUT/public_html/img/carousel/1.jpg">
                                <div class="carousel-caption">
                                </div>
                            </div>
                                                                                               <div class="item ">
                                <img src="http://localhost/IUT/public_html/img/carousel/2.jpg">
                                <div class="carousel-caption">
                                </div>
                            </div>
                                                                <!-- Controles -->
                    <a href="#carouselIUT" class="left carousel-control" role="button" data-slide="prev">
                        
                    </a>
                    <a href="#carouselIUT" class="right carousel-control" role="button" data-slide="next">
                        
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="container-fluid" style=" color: #FFF;">
    <div class="row ">
        <div class="col-sm-10 col-sm-offset-1" >
            
<br>
<center><h2 >Proximamente nuestra web...</h2>
    <br>
<h3> <i class="fa fa-twitter"></i> @IUT_FRP </h3>
</center> 
        </div>
        
    </div>
</div>

<script src="http://localhost/IUT/public_html/js/jquery-1.11.1.min.js"></script>

<script src="http://localhost/IUT/public_html/js/bootstrap.min.js"></script>

<script src="http://localhost/IUT/public_html/js/relacionAspecto.js"></script>

</body>
</html>